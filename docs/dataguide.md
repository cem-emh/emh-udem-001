# **CODERS DATA GUIDE**

- Version 2.0

- October 2023

- Hendriks, R., Aldana, D., Cusi, T., Ahmed, A. 

<!-- **Table of CONTENTS** -->
<!-- [TOC] -->
<!-- 


# 1. [Introduction](#introduction)

##    1.1. [Purpose](#purpose)

##    1.2. [Database Structure](#database-structure)

##    1.3. [Data Specifications](#data-specifications)

##    1.4. [Provincial Data](#provincial-data)

##    1.5. [Canadian Data](#canadian-data)

##    1.6. [Generic Data](#generic-data)

##    1.7. [Hourly Data](#hourly-data)

##    1.8. [Sources](#sources)

# 2.  [Data Management](#data-management)

##    2.1. [Entity Relationships](#entity-relationships)

##    2.2. [Business Rules](#business-rules)

##    2.2.1. [Application Oriented Rules](#application-oriented-rules)

##    2.2.2. [Database Oriented Rules](#database-oriented-rules)

# 3.  [Data Glossary](#data-glossary)

# 4.  [Sources](#sources-1) -->

	



## 1. **Introduction** 

Canadian Open-source Database for Energy Research and Systems-Modelling (CODERS) is an open-source database developed by the University of Victoria (Sustainable Energy Systems Integration and Transition Group, SESIT) and the University of Toronto (Dept. of Civil and Mineral Engineering). The recommended reference for CODERS is the following:

Hendriks, R.M., Monroe, J., Cusi, T., Aldana, D., Griffiths, K., Dorman, T., Chhina, A., McPherson, M. 2023\.  Canadian Open-source Database for Energy Research and Systems-Modelling (CODERS). DOI/URL: TBD.

## 1.1 **Purpose** 

This CODERS Data Guide describes the scope, definitions, assumptions, calculations, limitations, specifications, management, and sources of data contained within CODERS, and forms one component of the CODERS Manual. Included in this Data Guide are the following:

*  1\. Data Glossary – contains definitions for the data attributes within CODERS

*  2\. Data Specifications – relationships between data entities and attributes

*  3\. Data Management – rules and relationships guiding data entities and attributes within CODERS

*  4\. Data Sources – a table listing the primary sources of data in CODERS 

For more information on CODERS, including accessing the database, please consult the CODERS User Guide.

## 1.2 **Database Structure** 

The structure of the database is based on an entity relationship diagram (ERD). This ERD was designed by analysing the original data organized and provided in MS Excel workbooks. The ERD was created in MySQL Workbench as a model, and then converted by MySQL Workbench into a database schema. The schema defines tables based on the entities of the diagram and the data is filled in these tables. Tables maintain the relationships in the ERD via foreign keys. 

Below is the ERD showing the entities (blue boxes), relationships (red diamonds), attributes (yellow ovals) and the cardinalities between the entities. A data entity refers to a single object or concept in the real world about which information is stored in the database (e.g., generators, transmission lines, etc.). An attribute refers to a characteristic or trait of an entity type that describes the entity (e.g., installed capacity, average annual energy).

**Figure 1\. Database structure**

![A diagram of a computerDescription automatically generated][image1]

## 1.3 **Data Specifications** 

This section of the Data Guide specifies relations within CODERS between entities and attributes, and identifies the datatype associated with each attribute. A data entity refers to a single object or concept in the real world about which information is stored in the database (e.g., generators, transmission lines, etc.). Each data entity contains several attributes as summarized in the following tables.

The following define datatype terminology in the data tables:

*  Underline = Primary Key 

*  Bold = Foreign Key 

*  Varchar(n) = string of length n

*  Decimal(n,m) = number with n digits total and m digits after decimal point

*  TINYINT = \[signed, max:127, min:-128\] \[unsigned, max:255 , min:0\]

*  SMALLINT = \[signed, max:32767, min:-32768\] \[unsigned, max:65535 , min:0\]

*  MEDIUMINT = \[signed, max:8388607, min:-8388608\] \[unsigned, max:16777215 , min:0\]

*  INT =  \[signed, max:2147483647 , min:-2147483648\] \[unsigned, max:4294967295 , min:0\]

*  BOOLEAN = TRUE or FALSE (MySQL converts BOOLEAN to TINYINT internally)

*  DATETIME = Stored as  “yyyy-mm-dd hh-mm-ss”



## 1.4 **Provincial Data** 

| Generators (storage) |  |  |  |
| ----- | ----- | ----- | ----- |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/generators?province=AB&type=hydro\_daily |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Generation Unit Name | generation\_unit\_name |   | varchar(50) |
| Gen Unit Code | generation\_unit\_code |   | varchar(25) |
| Generation Facility Name | generation\_facility\_name |   | varchar(50) |
| Generation Facility Code | generation\_facility\_code |   | varchar(25) |
| Owner | owner |   | varchar(100) |
| Province | province |   | varchar(2) |
| Location | location |   | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Elevation | elevation | (m) | smallint |
| Balancing Area \- COPPER | copper\_balancing\_area |   | varchar(30) |
| Operating Region | operating\_region |   | varchar(30) |
| Connecting Node Name  | connecting\_node\_name |   | varchar(100) |
| Connecting Node Code | connecting\_node\_code |   | varchar(25) |
| Connecting Voltage | connecting\_voltage | (kV) | smallint |
| Start Year | start\_year |   | smallint |
| Previous Renewal Year | previous\_renewal\_year |   | smallint |
| Possible Renewal Year | possible\_renewal\_year |   | smallint |
| Closure Year | closure\_year |   | int |
| Generation Type | gen\_type |   | varchar(25) |
| Generation Type \- COPPER | gen\_type\_copper |   | varchar(25) |
| Unit Installed Capacity | unit\_installed\_capacity | (MW) | decimal(7,2) |
| Facility Installed Capacity | facility\_installed\_capacity | (MW) | decimal(7,2) |
| Capacity Adjustment | capacity\_adjustment | (%) | decimal(5,4) |
| Unit Effective Capacity | unit\_effective\_capacity | (MW) | decimal(7,2) |
| Facility Effective Capacity | facility\_effective\_capacity | (MW) | decimal(7,2) |
| Capacity Factor | capacity\_factor | (%) | decimal(5,4) |
| Unit Average Annual Energy | unit\_average\_annual\_energy | (GWh/year) | decimal(7,2) |
| Facility Average Annual Energy | facility\_average\_annual\_energy | (GWh/year) | decimal(7,2) |
| Total Number of Generation Units Represented | generation\_units\_represented |   | smallint |
| Total Number of Facility Generation Units | total\_facility\_generation\_units |   | smallint |
| **Storage (storage)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/storage\_batteries?province=AB |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Storage Facility Name | storage\_facility\_name |   | varchar(25) |
| Storage Facility Code | storage\_code |   | varchar(25) |
| Owner | owner |   | varchar(100) |
| Province | province |   | varchar(30) |
| Location | location |   | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Elevation | elevation | (m) | smallint |
| Balancing Area \- COPPER | copper\_balancing\_area |   | varchar(30) |
| Operating Region | operating\_region |   | varchar(30) |
| Connecting Node Name | connecting\_node\_name |   | varchar(25) |
| Connecting Node Code | connecting\_node\_code |   | varchar(25) |
| Connecting Voltage | connecting\_voltage\_in\_kv | (kV) | smallint |
| Start Year | start\_year |   | smallint |
| Previous Renewal Year | previous\_renewal\_year |   | smallint |
| Possible Renewal Year | possible\_renewal\_year |   | smallint |
| Closure Year | closure\_year |   | smallint |
| Storage Type | storage\_type |   | varchar(25) |
| Storage Type \- COPPER | storage\_type\_copper |   | varchar(25) |
| Storage Capacity | storage\_capacity | (MW) | smallint |
| Storage Energy | storage\_energy | (MWh) | smallint |
| Storage Duration | storage\_duration | (Hours) | decimal(4,2) |
| Associated Generation | associated\_generation |   | varchar(100) |
| **Hydroelectric Renewals (hydro\_renewal)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Renewal Project Name | renewal\_project\_name |   | varchar(50) |
| Project Code | renewal\_code |   | varchar(25) |
| Location | location |   | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Elevation | elevation | (m) | smallint |
| Balancing Area \- COPPER | copper\_balancing\_area |   | varchar(30) |
| Connecting Node Name | connecting\_node\_name |   | varchar(100) |
| Connecting Node Code | connecting\_node\_code |   | varchar(25) |
| Connecting Voltage | connecting\_voltage | (kV)  | int |
| Distance to Market Grid | distance\_to\_market\_grid | (km) | smallint |
| Generation Type | gen\_type |   | varchar(25) |
| Generation Type \- COPPER | gen\_type\_copper |   | varchar(25) |
| Additional Installed Capacity | additional\_installed\_capacity | (MW) | decimal(7,2) |
| Capacity Adjustment | capacity\_adjustment |  | decimal(5,4) |
| Additional Effective Capacity | additional\_effective\_capacity | (MW) | decimal(7,2) |
| Additional Annual Energy | additional\_annual\_energy | (GWh/year) | decimal(7,2) |
| Minimum Generation | minimum\_generation |  | decimal(5,2) |
| Total Capital Costs | capital\_costs | ($2021M) | decimal(7,2) |
| Capital Overhead | capital\_overhead | ($2021M) | decimal(7,2) |
| Interest During Construction | interest\_during\_construction | ($2021M) | decimal(7,2) |
| Implementation Costs | implementation\_costs | ($2021M) | decimal(7,2) |
| Project Definition Costs | project\_definition\_costs | ($2021M) | decimal(7,2) |
| Total Project Costs | total\_project\_costs | ($2021M) | decimal(7,2) |
| Annualized Project Costs | annualized\_project\_costs | ($M/year) | decimal(7,2) |
| Annualized Project Costs per kW | annualized\_project\_costs\_per\_kw | ($/kW-year) | decimal(7,2) |
| Fixed O&M Costs | fixed\_om\_costs | ($/MW-year) | decimal(8,2) |
| Variable O&M Costs | variable\_om\_costs | ($/MWh) | decimal(7,2) |
| Water Rentals | water\_rentals | ($/MWh) | decimal(7,2) |
| Construction Time | construction\_time | (years) | tinyint |
| Development Time | development\_time | (years) | tinyint |
| Notes | notes |  | text |
| **Hydroelectric Greenfield (hydro\_greenfield)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Project Name | greenfield\_facility\_name |   | varchar(50) |
| Project Code | greenfield\_code |   | varchar(25) |
| Owner | owner |   | varchar(100) |
| Location | location |  | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Elevation | elevation | (m) | smallint |
| Balancing Area \- COPPER | copper\_balancing\_area |   | varchar(30) |
| Distance to Market Grid | connecting\_node\_name | (km) | varchar(100) |
| Connecting Node Name | connecting\_node\_code |   | varchar(25) |
| Connecting Node Code | connecting\_voltage |   | int |
| Connecting Voltage | distance\_to\_market\_grid | (kV) | smallint |
| Generation Type | gen\_type |   | varchar(25) |
| Generation Type \- COPPER | gen\_type\_copper |   | varchar(25) |
| Installed Capacity | installed\_capacity | (MW) | decimal(7,2) |
| Effective Capacity | capacity\_adjustment | (MW) | decimal(5,4) |
| Average Annual Energy | effective\_capacity | (GWh/year) | decimal(7,2) |
| Capacity Factor | capacity\_factor | (%) | decimal(5,4) |
| Total Number of Facility Generation Units | facility\_average\_annual\_energy |   | decimal(7,2) |
| Total Capital Costs | generation\_units\_represented | ($2021M) | smallint |
| Capital Overhead | minimum\_generation | ($2021M) | decimal(5,2) |
| Interest During Construction | capital\_costs | ($2021M) | decimal(7,2) |
| Implementation Costs | capital\_overhead | ($2021M) | decimal(7,2) |
| Project Definition Costs | interest\_during\_construction | ($2021M) | decimal(7,2) |
| Total Project Costs | implementation\_costs | ($2021M) | decimal(7,2) |
| Annualized Project Costs | project\_definition\_costs | ($M/year) | decimal(7,2) |
| Annualized Project Costs per kW | total\_project\_costs | ($/kW-year) | decimal(7,2) |
| Fixed O&M Costs | annualized\_project\_costs | ($/MW-year) | decimal(7,2) |
| Variable O&M Costs | annualized\_project\_costs\_per\_kw | ($/MWh) | decimal(7,2) |
| Water Rentals | fixed\_om\_costs | ($/MWh) | decimal(8,2) |
| Construction Time | variable\_om\_costs | (years) | decimal(7,2) |
| Development Time | water\_rentals | (years) | decimal(7,2) |
| Notes | construction\_time |  | tinyint |
| **Hydroelectric Pumped Storage  (hydro\_pumped\_storage)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Storage Facility Name | storage\_facility\_name |  | varchar(50) |
| Storage Code | storage\_code |   | varchar(25) |
| Owner | owner |   | varchar(100) |
| Location | location |   | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Elevation | elevation | (m) | smallint |
| Balancing Area \- COPPER | copper\_balancing\_area |   | varchar(30) |
| Connecting Node Name | connecting\_node\_name |   | varchar(100) |
| Connecting Node Code | connecting\_node\_code |   | varchar(25) |
| Connecting Voltage | connecting\_voltage | (kV) | int |
| Distance to Market Grid | distance\_to\_market\_grid | (km) | smallint |
| Storage Type | storage\_type |   | varchar(25) |
| Storage Type \- COPPER | storage\_type\_copper |   | varchar(25) |
| Installed Capacity | installed\_capacity | (MW) | decimal(7,2) |
| Effective Capacity | effective\_capacity | (MW) | decimal(7,2) |
| Round Trip Efficiency | round\_trip\_efficiency | (%) | decimal(5,2) |
| Minimum Generation | minimum\_generation |  | decimal(5,2) |
| Generation Facility Name | existing\_facility\_generation\_facility\_name |  | varchar(50) |
| Generation Type | existing\_facility\_gen\_type |   | varchar(25) |
| Installed Capacity | existing\_facility\_installed\_capacity | (MW) | decimal(7,2) |
| Capacity Factor | existing\_facility\_capacity\_adjustment | (%) | decimal(5,4) |
| Effective Capacity | existing\_facility\_effective\_capacity | (MW) | decimal(7,2) |
| Capacity Factor | existing\_facility\_capacity\_factor | (%) | decimal(5,4) |
| Facility Average Annual Energy | existing\_facility\_facility\_average\_annual\_energy | (GWh/year) | decimal(7,2) |
| Total Number of Facility Generation Units | existing\_facility\_total\_facility\_generation\_units |   | smallint |
| Water Type | water\_type |   | varchar(25) |
| Cycle Type | cycle\_type |   | varchar(25) |
| Reservoir Type | reservoir\_type |   | varchar(25) |
| Reservoir Storage | reservoir\_storage | (m3) | bigint |
| Storage Energy | storage\_energy | (MWh) | mediumint |
| Gross Head | gross\_head | (m) | mediumint |
| Penstock Length | penstock\_length | (m) | smallint |
| Design Flow (Generation) | design\_flow\_generation | (m3/s) | smallint |
| Design Flow (Pumping) | design\_flow\_pumping | (m3/s) | smallint |
| Turbine Type | turbine\_type |   | varchar(25) |
| Pump Type | pump\_type |   | varchar(25) |
| Generation Duration at Peak Output  | generation\_duration\_at\_peak\_output | (h) | tinyint |
| Pumping Duration at Peak Output | pumping\_duration\_at\_peak\_output | (h) | tinyint |
| Project Footprint | project\_footprint | (ha) | decimal(5,2) |
| Capital Costs | capital\_costs | ($2021M) | decimal(7,2) |
| Capital Overhead | capital\_overhead | ($2021M) | decimal(7,2) |
| Interest During Construction | interest\_during\_construction | ($2021M) | decimal(7,2) |
| Implementation Costs | implementation\_costs | ($2021M) | decimal(7,2) |
| Project Definition Costs | project\_definition\_costs | ($2021M) | decimal(7,2) |
| Total Project Costs | total\_project\_costs | ($2021M) | decimal(7,2) |
| Annualized Project Costs | annualized\_project\_costs | ($M/year) | decimal(7,2) |
| Annualized Project Costs per KW | annualized\_project\_costs\_per\_kw | ($/kW-year) | decimal(7,2) |
| Fixed O&M Costs | fixed\_om\_costs | ($/MW-year) | decimal(8,2) |
| Variable O&M Costs | variable\_om\_costs | ($/MWh) | decimal(7,2) |
| Water Rentals | water\_rentals | ($/MWh) | decimal(7,2) |
| Construction Time | construction\_time | (years) | tinyint |
| Development Time | development\_time | (years) | tinyint |
| Notes | notes |  | text |
| **Existing Wind Generation (wind\_generators)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Generation Facility Name | generation\_facility\_name |   | varchar(50) |
| Gen Code | generation\_facility\_code |   | varchar(25) |
| Owner | owner |   | varchar(100) |
| Province | province |   | varchar(30) |
| Location | location |   | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Elevation | elevation | (m) | smallint |
| Balancing Area \- COPPER | copper\_balancing\_area |   | varchar(30) |
| Operating Region | operating\_region |   | varchar(30) |
| Connecting Node Name | connecting\_node\_name |   | varchar(100) |
| Connecting Node Code | connecting\_node\_code |   | varchar(25) |
| Connecting Voltage | connecting\_voltage | (kV) | int |
| Start Year | start\_year |   | smallint |
| Previous Renewal Year | previous\_renewal\_year |   | smallint |
| Possible Renewal Year | possible\_renewal\_year |   | smallint |
| Closure Year | closure\_year |   | smallint |
| Generation Type | gen\_type |   | varchar(25) |
| Generation Type \- COPPER | gen\_type\_copper |   | varchar(25) |
| Installed Capacity | facility\_installed\_capacity | (MW) | int |
| Capacity Adjustment | capacity\_adjustment |   | decimal(5,4) |
| Effective Capacity | facility\_effective\_capacity | (MW) | int |
| Capacity Factor | capacity\_factor | (%) | decimal(5,4) |
| Annual Average Energy | average\_annual\_energy | (GWh/year) | decimal(7,2) |
| Number of Generation Units Represented | generation\_units\_represented |   | smallint |
| Total Number of Facility Generation Units | total\_facility\_generation\_units |   | smallint |
| Turbine Capacity | wind\_turbine\_capacity | (MW) | decimal(5,2) |
| Project Area | project\_area | (km2) | decimal(5,2) |
| Density | density | (MW/km2) | decimal(5,2) |
| Notes | notes |  | varchar(250) |
| **Transmission (transmission\_lines)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Unique line ID | transmission\_line\_id |  | int |
| Transmission Circuit ID | transmission\_circuit\_id |   | varchar(25) |
| Owner | owner |   | varchar(100) |
| Province | province |   | varchar(30) |
| Operating Region | operating\_region |   | varchar(100) |
| Number of Circuits | number\_of\_circuits |   | tinyint |
| Current | current\_type | (aC/dc) | varchar(2) |
| Transmission Line Segment Length | line\_segment\_length\_km | (km) | decimal(7,3) |
| Transmission Line Segment Length | line\_segment\_length\_mi | (miles) | decimal(7,3) |
| Transmission Line Length | line\_length\_km | (km) | decimal(7,3) |
| Transmission Line Length | line\_length\_mi | (miles) | decimal(7,3) |
| Voltage | voltage | (kV) | smallint |
| Reactance | reactance | (ohm) | decimal(5,2) |
| Summer Capacity | summer\_capacity | (MW) | smallint |
| Winter Capacity | winter\_capacity | (MW) | smallint |
| Starting Node Name | starting\_node\_name |   | varchar(45) |
| Starting Node Code | starting\_node\_code |   | varchar(25) |
| Ending Node Name | ending\_node\_name |   | varchar(45) |
| Ending Node Code | ending\_node\_code |   | varchar(25) |
| Notes | notes |  | varchar(250) |
| **Nodes (nodes)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Node Name | node\_name |   | varchar(100) |
| Node Code | node\_code |   | varchar(25) |
| Node Type | node\_type |   | varchar(45) |
| Owner | owner |   | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Elevation | elevation | (m) | smallint |
| Province | province |   | varchar(30) |
| Operating Region | operating\_region |   | varchar(100) |
| Balancing Area \- COPPER | copper\_balancing\_area |   | varchar(30) |
| Notes | notes |  | varchar(250) |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |

## 1.5 **Canadian Data** 

| Contracts (contracts) |  |  |  |
| ----- | :---: | :---: | ----- |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Contract Name | contract\_name |   | varchar(50) |
| Contract Code | contract\_code |   | varchar(50) |
| Seller | seller |   | varchar(100) |
| Buyer | buyer |   | varchar(100) |
| Country From | country\_from |  | varchar(25) |
| Country To | country\_to |  | varchar(25) |
| Province or state from | province\_state\_from |   | varchar(50) |
| Province or state to | province\_state\_to |  | varchar(50) |
| Location | location |   | varchar(100) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Balancing Area \- COPPER from | balancing\_area\_from |   | varchar(30) |
| Balancing Area \- COPPER to | balancing\_area\_to |  | varchar(30) |
| Connecting Node Name | connecting\_node\_name |   | varchar(100) |
| Connecting Node Code | connecting\_node\_code |   | varchar(35) |
| Connecting Voltage | connecting\_voltage | (kV) | smallint |
| Start Year | start\_year |   | smallint |
| Previous Renewal Year | previous\_renewal\_year |   | smallint |
| Possible Renewal Year | possible\_renewal\_year |   | smallint |
| Closure Year | closure\_year |   | int |
| Generation Type | gen\_type |   | varchar(25) |
| Generation Type \- COPPER | gen\_type\_copper |   | varchar(25) |
| Import Export Capacity | import\_export\_capacity | (MW) | smallint |
| Capacity Adjustment | capacity\_adjustment | (%) | decimal(5,2) |
| Effective Capacity | effective\_capacity | (MW) | decimal(7,2) |
| Capacity Factor | capacity\_factor | (%) | decimal(5,2) |
| Average Annual Energy | average\_annual\_energy | (GWh/year) | decimal(7,2) |
| Notes | notes |  | varchar(1000) |
| **Transfer Capacities – copper balancing areas (transfer\_capacities\_copper)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| From Balancing Area \- COPPER | balancing\_area\_from |   | varchar(30) |
| To Balancing Area \- COPPER | balancing\_area\_to |   | varchar(30) |
| COPPER CODE | copper\_code |   | varchar(200) |
| TTC \- Summer | ttc\_summer | (MW)  | mediumint |
| TTC \- Winter | ttc\_winter | (MW)  | mediumint |
| TTC \- Lower | ttc\_lower | (MW)  | mediumint |
| Other References | other\_references |   | varchar(2000) |
| Notes | notes |   | varchar(250) |
| **Transfer Capacities – jurisdictions (interface\_capacities)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| From Country | country\_from |   | varchar(100) |
| To Country | country\_to |   | varchar(100) |
| From Province or State | province\_state\_from |   | varchar(100) |
| To Province or State | province\_state\_to |   | varchar(100) |
| Current | current |   | varchar(5) |
| Summer Capacity | summer\_capacity | (MW)  | mediumint |
| Winter Capacity | winter\_capacity | (MW) | mediumint |
| Associated Interties | associated\_interties |   | varchar(100) |
| Other References | other\_references |  | varchar(2000) |
| Notes | notes |   | varchar(250) |
| **Transfer Capacities – international lines** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Data guide name** | **API name** | **Units** | **Datatype** |
| Node Name | node\_name |   | varchar(50) |
| Node Code | node\_code |   | varchar(50) |
| Transmission Circuit ID | associated\_circuit\_id |   | varchar(50) |
| Owner | owner |   | varchar(100) |
| From Country | country\_from |   | varchar(25) |
| To Country | country\_to |   | varchar(25) |
| To Province or State | province\_state\_from |   | varchar(50) |
| From Province or State | province\_state\_to |   | varchar(50) |
| Latitude | latitude | (dd.dddd) | decimal(9,6) |
| Longitude | longitude | (dd.dddd) | decimal(9,6) |
| Number of Circuits | num\_of\_circuits |   | tinyint |
| Voltage | line\_voltage | (kV) | smallint |
| Intertie Status | status |   | varchar(50) |
| Summer Total Transfer Capacity | summer\_capacity | (MW) | smallint |
| Winter Total Transfer Capacity | winter\_capacity | (MW) | smallint |
| Notes | notes |   | varchar(1000) |

## 1.6 **Generic Data** 

| Generation – Generic (generation\_generic) |  |  |  |
| ----- | ----- | ----- | ----- |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Attribute** | **API name** | **Units** | **Datatype** |
| Generation Type | gen\_type |   | varchar(50) |
| Generation Type \- COPPER | gen\_type\_copper |   | varchar(50) |
| Description | description |   | varchar(100) |
| Typical Plant Size | typical\_plant\_size\_MW | (MW) | smallint |
| Capital Costs | capital\_cost\_CAD\_per\_kW | ($CAD/kW) | decimal(8,2) |
| Capital Overhead | capital\_overhead\_CAD\_per\_kW | ($CAD/kW) | decimal(7,2) |
| Overnight Capital Costs | overnight\_capital\_cost\_CAD\_per\_kW | ($CAD/kW) | decimal(7,2) |
| Interest During Construction | interest\_during\_construction\_CAD\_per\_kW | ($CAD/kW) | decimal(7,2) |
| Implementation Costs | implementation\_costs\_CAD\_per\_kW | ($CAD/kW) | decimal(7,2) |
| Project Definition Costs | project\_definition\_costs\_CAD\_per\_kW | ($CAD/kW) | decimal(7,2) |
| Total Project Costs (2020) | total\_project\_cost\_2020\_CAD\_per\_kW | ($CAD/kW) | decimal(7,2) |
| Economic Life (years) | economic\_life | (years) | tinyint |
| Service Life | service\_life | (years) | tinyint |
| Annualized Capital Cost | annualized\_capital\_cost\_CAD\_per\_MWyear | ($CAD/MW-year) | decimal(9,2) |
| Fixed O&M Costs | fixed\_om\_cost\_CAD\_per\_MWyear | ($CAD/MW-year) | mediumint |
| Variable O&M Costs | variable\_om\_cost\_CAD\_per\_MWh | ($CAD/MWh) | decimal(5,2) |
| Construction Time | construction\_time | (years) | tinyint |
| Development Time | development\_time | (years) | tinyint |
| Average Fuel Price | average\_fuel\_price\_CAD\_per\_MMBtu | ($CAD/GJ) | decimal(5,2) |
| Carbon Emissions | average\_fuel\_price\_CAD\_per\_GJ | (lbs/MMBtu) | decimal(5,2) |
| Carbon Emissions | carbon\_emissions | (tCO2eq/MWh) | decimal(7,6) |
| Heat Rate | heat\_rate | (MMBtu/MWh) | decimal(5,3) |
| Generation Efficiency | efficiency | (%) | decimal(5,4) |
| Minimum Plant Load | min\_plant\_load | (%) | decimal(5,4) |
| Minimum Capacity Factor | min\_capacity\_factor | (%) | decimal(5,4) |
| Maximum Capacity Factor | max\_capacity\_factor | (%) | decimal(5,4) |
| Time to Full Capacity | time\_to\_full\_capacity | (minutes) | smallint |
| Minimum Up Time | min\_up\_time\_hours | (hours) | tinyint |
| Minimum Down Time | min\_down\_time\_hours | (hours) | tinyint |
| Ramp Rate | ramp\_rate\_percent\_per\_min | (%/min) | decimal(5,4) |
| Spinning Reserve Capability | spinning\_reserve\_capability | (%) | decimal(5,4) |
| Forced Outage Rate | forced\_outage\_rate | (%) | decimal(5,4) |
| Planned Outage Rate | planned\_outage\_rate | (%) | decimal(5,4) |
| Cold Start Up Costs | startup\_cost | ($/MWcap) | smallint |
| Shut Down Costs | shutdown\_cost | ($/MWcap) | decimal(5,2) |
| Other References | other\_references |  | varchar(2000) |
| Notes | notes |   | varchar(1000) |
| **Natural Gas Prices Monthly (natural\_gas\_prices)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Attribute** | **API name** | **Units** | **Datatype** |
| Month | month |   | varchar(3) |
| Year | year |   | smallint |
| Average Monthly Price | average\_monthly\_price\_USD\_per\_MMBtu | (USD/MMBTU) | decimal(5,2) |
| **Natural Gas Prices Annual (natural\_gas\_prices\_annual)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Attribute** | **API name** | **Units** | **Datatype** |
| year |  |   | smallint |
| average annual price | average\_annual\_price\_USD\_per\_MMBtu | USD/MMBTU | decimal(5,2) |
| **Transmission \- Generic (transmission\_generic)** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Attribute** | **API name** | **Units** | **Datatype** |
| Transmission Type | transmission\_type |   | varchar(50) |
| Current Type | current\_type |   | varchar(10) |
| Number of Circuits | number\_of\_circuits |   | smallint |
| Transfer Capacity | transfer\_capacity | (MW) | smallint |
| Distance | distance | (km) | smallint |
| Voltage | voltage | (kV) | smallint |
| Capital Costs | capital\_cost\_CAD\_per\_km | ($2021CAD/km) | int |
| Capital Costs | capital\_cost\_CAD | ($2021CAD) | decimal(12,2) |
| Capital Costs / MW | capital\_cost\_CAD\_per\_MW | ($2021CAD/MW) | decimal(11,2) |
| Capital Costs / km | capital\_cost\_MCAD\_per\_km | ($M2021CAD/km) | int |
| Economic Life | economic\_life | (years) | tinyint |
| Annualized Project Costs | annualized\_project\_costs\_CAD\_per\_MWyear | ($2021CAD/MW-year) | decimal(8,2) |
| Annualized Project Costs Per km | annualized\_project\_cost\_CAD\_per\_MWkmyear | ($2021CAD/MW-km-year) | decimal(6,2) |
| Fixed O&M Costs | fixed\_om\_CAD\_per\_MWkmyear | ($2021CAD/MW-km-year) | decimal(12,2) |
| Construction Time | construction\_time | (years) | tinyint |
| Development Time | development\_time | (years) | tinyint |
| Other References | other\_references |  | varchar(1000) |

## 1.7 **Hourly Data** {#hourly-data}

| International Hourly Transfers (international\_transfers) |  |  |  |
| ----- | :---: | :---: | ----- |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Attribute** | **API name** | **Units** | **Datatype** |
| Local Time | local\_time |   | datetime |
| Province | province |   | varchar(30) |
| US State | us\_state |   | varchar(45) |
| Daily Hour Ending | daily\_hour\_ending |   | tinyint |
| Annual Hour Ending | annual\_hour\_ending |   | smallint |
| Transfers | transfers\_MWh | (MW)  | mediumint |
| Price | price\_usd | (USD)  | decimal(6,2) |
| Exchange Rate | exchange\_rate | (%)  | decimal(6,4) |
| Price  | price\_cad | (CAD)  | decimal(6,2) |
| **Interprovincial Hourly Transfers** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Attribute** | **API name** | **Units** | **Datatype** |
| Local Time | local\_time |   | datetime |
| Province 1 | province\_1 |   | varchar(20) |
| Province 2 | province\_2 |   | varchar(20) |
| Daily Hour Ending | daily\_hour\_ending |   | tinyint |
| Annual Hour Ending | annual\_hour\_ending |   | smallint |
| Transfers | transfers\_MWh | (MW)  | mediumint |
| **Provincial Hourly Demand** |  |  |  |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |  |
| **Attribute** | **API name** | **Units** | **Datatype** |
| Local Time | local\_time |  | datetime |
| Province | province |   | varchar(20) |
| Daily Hour Ending | daily\_hour\_ending |   | tinyint |
| Annual Hour Ending | annual\_hour\_ending |   | smallint |
| Demand | demand\_MWh | (MW)  | mediumint |

## 1.8 **Sources** {#sources}

| sources |  |  |
| ----- | ----- | ----- |
| Example from CODERS-Manual-UserGuide: https://api.sesit.ca/ |  |  |
| **Attribute** | **Units** | **Datatype** |
| id |   | INT |
| Source |   | Varchar(250) |
| Year |   | SMALLINT |
| Name |   | Varchar(50) |
| Link |   | Varchar(1000) |
| Publication |   | Varchar(250) |
| Region |   | Varchar(30) |
| Notes |   | Varchar(500) |

## 2 **Data Management** {#data-management}

This section of the Data Guide provides further details related to data entities, attributes, relationships, and management.

## 2.1 **Entity Relationships** {#entity-relationships}

The following relationship rules pertain to data entities and attributes:

*  Each Generator can connect to one Node (Substation or Intertie). 

* Related by node code

*  Each Node can connect to 0 or many Generators

* Related by node code

*  Each Transmission line connects exactly two nodes (Substation, Junction, or Intertie). 

* Related by node code

*  Each Node can connect to many Transmission lines. 

* Related by node code

*  Each Storage can connect to one Node (Substation). 

* Related by node code

*  Each Node can connect to many Storages.

* Related by node code

*  Each Node can have many Distribution Transfers

* Related by node code

*  Each Distribution Transfer belongs to one Node (Substation)

* Related by node code

*  Each Generator can have many Contribution Transfers

* Related by node code

*  Each Contribution Transfer belongs to one Generator

* Related by node code

*  Each Interface Node connects one Interface Capacity Entry to one or many Nodes (intertie)

* Related by node code and exporter/importer relationship

## 2.2 **Business Rules** {#business-rules}

Business rules include application-oriented rules and database-oriented rules. Application-oriented rules are those that go beyond the scope of the database design and apply to the physical implementation or the application of the database. Database-oriented rules are those that directly affect how the database schema is designed and generally act as constraints within the database.

## 2.3 **Application Oriented Rules** {#application-oriented-rules}

*  Generation Facilities, Generation Units, Storage Facilities, Nodes, and Transmission Lines must be actively operational or planned for future operation.

*  All data within CODERS relates to the interconnected Canadian electricity transmission system. Remote electricity facilities, facilities connected to the distribution system and facilities in the United States are excluded from CODERS.

## 2.4 **Database Oriented Rules** {#database-oriented-rules}

*  **Nodes** – A node must have a unique Node Code.

*  **Substations –** A substation must have a unique Node Code that correlates to an existing entry in the Nodes table.

*  **Substations –** A substation must be added after its correlating Node entry.

*  **Junctions –** A junction must have a unique Node Code that correlates to an existing entry in the Nodes table.

*  **Junctions –** A junction must be added after its correlating Node entry.

*  **Interties –** An intertie must have a unique Node Code which correlates to an existing entry in the Nodes table.

*  **Interties –** An intertie must be added after its correlating Node entry.

*  **Generators –** A Generation Facility or Generation Unit must have a Connecting Node to the system.

*  **Generators –** A Generation Facility or Generation Unit must be added after the Connecting Node is added.

*  **Generators –** A Generation Facility or Generation Unit must connect only to a single Node.

*  **Generators –** A Generation Facility or Generation Unit can have multiple contribution transfers associated with it.

*  **Storages –** A Storage Facility must have a Connecting Node to the system.

*  **Storages –** A Storage Facility must be added after the Connecting Node is added.

*  **Storages –** A Storage Facility must connect to a single Node.

*  **Transmission Lines –** A Ttransmission Lline Segment must have a Starting Node and Ending Node, and both Nodes must have an existing entry in the Nodes table.

## 2.5 **Data Glossary** {#data-glossary}

The database includes primarily electricity system data related to generation, storage, transmission, substations and system operations, hourly and annual provincial electricity demand data, as well as interprovincial and international electricity transfers and transmission interties.

This glossary defines the data entities and attributes contained within CODERS:

*  Definition: the meaning of the entity or attribute

*  Clarifications: aspects of the definition that may require further explanation or clarification

*  Calculations: where relevant, sample calculations undertaken to obtain data values for the attribute or entity

*  Limitations: aspects of the calculations that may require further explanation or clarification and that may limit the interpretation of the data values

*  Sources: the source id number(s) for sources used to obtain or support calculation of the data. A table description of source id numbers can be found in [section 5](#sources-1).

   


| Annualized Project Costs ($M/year) |  |
| :---- | :---- |
| Definition | The Total Project Costs of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility annualized over the facility’s economic life (70 years) at a real discount rate of 7%. |
| Clarifications | None. |
| Calculations | Annualized Project Costs \= (Total Project Costs\*Interest Rate)/(1-(1+Interest Rate)^-Economic Life), where Interest Rate \= 7% Economic Life \= 70 years |
| Limitations | Interest rates and economic life may vary from values assumed. |
| Sources | See Capital Costs |

| Annualized Project Costs per kW ($M/kW-year) |  |
| :---- | :---- |
| Definition | The Total Project Costs of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility annualized over the facility’s economic life (70 years) at a real discount rate of 7% in terms of kW of installed capacity. |
| Clarifications | None. |
| Calculations | Annualized Project Costs per kW \= Annualized Project Costs\*1000/Installed Capacity |
| Limitations | Interest rates and economic life may vary from values assumed. |
| Sources | See Capital Costs |

| Associated Generation |  |
| :---- | :---- |
| Definition | The generation, usually wind or solar, that is developed in conjunction with or associated with charging a hybrid storage facility. |
| Clarifications | With exception of Summerview 1 (AB\_SUM01\_STO), all Storage Facilities are grid supplied. This attribute is included in CODERS as more Associated Generation is currently planned for future Storage Facilities |
| Calculations | No calculations. |
| Limitations | None. |
| Sources | N/A |

| Associated Interties |  |
| :---- | :---- |
| Definition | Transmission interties composing the total transfer capacity between jurisdictions (e.g., Province to Province). |
| Clarifications | None. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | 37 |

| Available Transfer Capacity (MW) |  |
| :---- | :---- |
| Definition | The transfer capacity of a transmission line after deducting firm reservations from Total Transfer Capacity. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | N/A |

| Average Fuel Price ($/GJ) |  |
| :---- | :---- |
| Definition | The annual average price of a fuel required to power a thermal Generation Facility. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 9, 45, 49, 50 |

| Balancing Area – COPPER |  |
| :---- | :---- |
| Definition | The balancing area within the COPPER model in which a generation, storage, transmission, or other facilities operate. |
| Clarifications | Some facilities are physically located in one province but interconnected to and operating within a neighbouring province. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | 62, 244 |

| Capacity Adjustment (%) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The percent by which the Installed Capacity of a generation unit or facility is derated based on its electric load carrying capability or forced outage rates to obtain its Effective Capacity.  |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained or derived from sources. Where derived, the Capacity Adjustment is obtained by dividing the Effective Capacity by the Installed Capacity. |  |  |  |
| Limitations |  |  |  |  |
| Sources | AB | 28, 98, 192 | NS | 28, 98, 192, 205 |
|  | BC | 28, 98, 192 | ON | 28, 98, 102 |
|  | MB | 28, 98, 140, 192 | PE | 28, 98, 130, 192 |
|  | NB | 28, 98, 192, 198 | QC | 28, 87, 98, 192 |
|  | NL | 165, 212 | SK | 28, 87, 140, 192 |

| Capacity Factor (%) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The ratio of actual electrical energy output of a generation unit or facility over a year to the theoretical maximum electrical energy output resulting from its continuous operation at full installed capacity over that year. |  |  |  |
| Clarifications | None |  |  |  |
| Calculations | Capacity Factor \= Average Annual Energy / Installed Capacity\*365\*24\*1000 |  |  |  |
| Limitations | Average annual energy can and does vary substantially from year to year due to environmental, system operating and demand factors. |  |  |  |
| Sources | AB | 4, 12, 28 | NS | 176, 189, 205 |
|  | BC | 28, 29, 243, 244 | ON | 101 |
|  | MB | 109, 113, 117, 120 | PE | 53, 128, 130, 192 |
|  | NB | 44, 53, 105, 150, 152 | QC | 79, 158, 192, 214 |
|  | NL | 161, 165, 168, 212 | SK | 220 |

| Capital Costs ($M) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The estimated capital cost of a generation facility including contingency and project management costs, but excluding interest during construction (IDC), corporate overhead, future inflation, and transmission costs. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | Estimates of capital costs are obtained from sources which vary in their use of cost estimation methodologies and estimation class. The level of uncertainty varies between capital cost estimates for different projects. |  |  |  |
| Sources | AB | 3, 11, 38, 238 | NS | N/A |
|  | BC | 28  | ON | 74 |
|  | MB | 113 | PE | N/A |
|  | NB | 148, 154 | QC | N/A |
|  | NL | 165 | SK | 232 |

| Capital Overhead ($M) |  |
| :---- | :---- |
| Definition | The estimated cost of certain supporting functions which are capitalized and charged to a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation capital projects. |
| Clarifications | Supporting functions typically include: corporate services, senior management oversight, supervision, project governance, accounting, post-employment benefits, and dedicated health and safety resources.  |
| Calculations | A value of 2% of Capital Cost is used based on BC Hydro’s standard rate for capital overhead costs. |
| Limitations | Since the capital overhead rate is typically calculated by dividing the capital overhead cost pool by the total direct labour transfers made to capital projects, actual capital overhead costs for specific projects may vary somewhat from those estimated. |
| Sources | See Capital Costs |

| Carbon Emissions (tCO2eq/MWh) |  |
| :---- | :---- |
| Definition | The carbon dioxide equivalent emissions of the operations of Generation Facilities. |
| Clarifications | Carbon Emissions does not include embedded emissions from construction or materials related to Generation Facilities. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 201 |

| Closure Year |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The year a generation or storage facility was closed or is scheduled to be closed, or the year a contract for import/export terminated or is scheduled to terminate. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 5, 10 | NS | 177, 187, 191 |
|  | BC | 28, 29 | ON | 103, 206 |
|  | MB | 120 | PE | 130 |
|  | NB | 148, 149, 156 | QC | 33, 236 |
|  | NL | 165 | SK | 220, 226 |

| Cold Start Up Costs ($/MW Cap) |  |
| :---- | :---- |
| Definition | The cost for a Generation Facility to ramp from idle (cold) conditions to full installed generation capacity, expressed as a ratio of total costs divided by installed capacity. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 202, 243 |

| Connecting Node Name |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | Transmission system node (substation) to which a facility is interconnected. In the case of a distribution-connected facility, the proximal transmission system node to the distribution-connected facility. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | Distribution-connected generation facilities may connect to the system such that their generation enters the transmission system at more than one node. |  |  |  |
| Sources | AB | 3 | NS | 180, 191 |
|  | BC | 30, 55, 65 | ON | 77 |
|  | MB | 108 | PE | 130 |
|  | NB | 150, 151, 231 | QC | 88 |
|  | NL | 144, 160, 166 | SK | 226, 227 |

| Connecting Node Code |  |
| :---- | :---- |
| Definition | The unique CODERS asset identification code defining each transmission system node and consisting of the following separated by an underscore: A two-letter provincial code A three-letter to five-letter generation facility code A three-digit code defining the node |
| Clarifications | Transmission system nodes are defined as one of the following: CSS \= AC/DC Converter Station DSS \= Distribution Substation GSS \= Generation Substation INT \= International Intertie IPT \= Interprovincial Intertie ISS \= Industrial Customer Substation JCT \= Junction SWS \= Switching Station TSS \= Terminal Station |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | N/A |

| Connecting Voltage (kV) |  |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
| Definition | The voltage at which a Generation Facility, Generation Unit or Storage Facility is known or assumed to be interconnected to the transmission network. |  |  |  |  |
| Clarifications | None |  |  |  |  |
| Calculations | None |  |  |  |  |
| Limitations | The connecting voltage is not known in all instances and is sometimes presumed based on the known voltage of nearest transmission line. |  |  |  |  |
| Sources |  | AB | 3 | NS | 191 |
|  |  | BC | 30, 55, 65 | ON | 77 |
|  |  | MB | 108 | PE | 130 |
|  |  | NB | 150, 151, 231 | QC | 88, 89 |
|  |  | NL | 144, 160, 166 | SK | 226, 227 |

| Construction Time (Years) |  |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
| Definition | The duration (in years) for completion of construction of a Generation Facility, Storage Facility or Transmission Facility beginning with the start of construction and ending with commissioning of the final generation unit. |  |  |  |  |
| Clarifications | Construction Time is reported to the nearest higher year where a range of estimates is provided from a source (e.g., 8 to 9 years is reported in CODERS as 9 years). |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |  |
| Limitations | Actual construction periods, particularly for capital intensive projects such as hydroelectric and nuclear, often exceed estimated Construction Time. |  |  |  |  |
| Sources |  | AB | 28 | NS | N/A |
|  |  | BC | 15, 18, 28 | ON | 74 |
|  |  | MB | 113 | PE | N/A |
|  |  | NB | 148, 154 | QC | N/A |
|  |  | NL | 141, 165 | SK | 232 |

| Contract Name |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The CODERS name given to a contract for capacity (and in some instances energy) sales between provinces or between a province and the United States. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | No calculations. |  |  |  |
| Limitations | Contracts for seasonal capacity exchanges are not included in CODERS. |  |  |  |
| Sources | AB | N/A | NS | 186 |
|  | BC | N/A | ON | 83 |
|  | MB | 123 | PE | 148 |
|  | NB | 148 | QC | 83, 88 |
|  | NL | 186, 212 | SK | 123 |

| Contract Code |  |
| :---- | :---- |
| Definition | The unique CODERS asset identification code defining each Contract and consisting of the following separated by an underscore: A two-letter provincial code indicating sales between provinces (“PP”) or exports to the United States (“XX”) A six-letter code consisting of the two-digit code of the exporting province, the two-digit code of the importing province and a two-digit numerical code numbering the contracts between jurisdictions A three digit “CON” defining this as a contract |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | Contracts for seasonal capacity exchanges are not included in CODERS. |
| Sources | N/A |

| Current Type |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The type of current (alternating current or direct current) associated with a transmission line or transmission line segment. |  |  |  |
| Clarifications | All lines are presumed to be alternating current unless specific information is located to indicate they are direct current. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 3 | NS | 180, 184 |
|  | BC | 30, 55 | ON | 78 |
|  | MB | 109, 111, 112, 114, 115, 116, 118, 121, 122, 138 | PE | 130 |
|  | NB | 105, 150, 231 | QC | 88 |
|  | NL | 144, 166, 212 | SK | 216 |

| Cycle Type |  |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
| Definition | The type of cycle (open or closed) used in a pumped hydroelectric Storage Facility. |  |  |  |  |
| Clarifications | An open cycle contains at least one reservoir that discharges or receives water to or from the environment.   |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |  |
| Limitations | None. |  |  |  |  |
| Sources |  | AB | 238, 240 | NS | N/A |
|  |  | BC | 19 | ON | 174, 233 |
|  |  | MB | N/A | PE | N/A |
|  |  | NB | 148 | QC | N/A |
|  |  | NL | N/A | SK | N/A |

| Design Flow – Generation (m3/s) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The maximum water flow rate of the total installed generation capacity of a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications | The design flow is inclusive of all Generation Units in a pumped hydroelectric Storage Facility. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 238, 240 | NS | N/A |
|  | BC | 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Design Flow – Pumping (m3/s) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The maximum water flow rate of the total installed pumping capacity of a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications | The design flow is inclusive of all pumping units in a pumped hydroelectric Storage Facility. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 238, 240 | NS | N/A |
|  | BC | 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Development Time (Years) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The duration (in years) for completion of development of a Generation Facility, Storage Facility or Transmission Facility beginning with the submission of a project description to regulatory bodies and ending with commissioning of the final generation unit. |  |  |  |
| Clarifications | Development Time is reported to the nearest higher year where a range of estimates is provided from a source (e.g., 8 to 9 years is reported in CODERS as 9 years). |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | Actual development periods, particularly for capital intensive projects such as hydroelectric and nuclear, often exceed estimated Development Time. |  |  |  |
| Sources | AB | 28 | NS | N/A |
|  | BC | 15, 18, 28 | ON | 74 |
|  | MB | 113 | PE | N/A |
|  | NB | 148, 154 | QC | N/A |
|  | NL | 141, 165 | SK | 232 |

| Distance to Market Grid (km) |  |
| :---- | :---- |
| Definition | The estimated distance of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility to a feasible transmission interconnection point on the interconnected transmission grid. |
| Clarifications | Unless already included in total capital costs, 25 km is added to Distance to Market Grid to account for potential grid interconnection costs. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | Distance to Market Grid does not currently include specific estimates of costs for grid interconnection, including substation development. |
| Sources | 62 |

| Economic Life (years) |  |
| :---- | :---- |
| Definition | The time that a Generation Facility, Storage Facility, or other asset is anticipated to provide economic benefits to the facility owner. |
| Clarifications | Economic benefits refer to the net benefits of revenues less operation, maintenance, and other costs. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 17, 203, 205, 237 |

| Elevation (m) |  |
| :---- | :---- |
| Definition | The physical elevation above sea level in which a generation, storage, transmission, or other facility is physical located. |
| Clarifications | None. |
| Calculations | Latitude and longitude are used to sample elevations from a digital elevation model (DEM) using GIS software. |
| Limitations | Minor errors in latitude and longitude coupled with limitations in the accuracy of the DEM may result in some discrepancy (est. \+/- 10 m) between actual and estimated elevations.  |
| Sources | 181 |

|  | Facility Average Annual Energy (GWh/year) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
|  | Definition | The actual annual electrical energy output of a Generation Facility averaged over more than one year depending on data availability. |  |  |  |
|  | Clarifications | Some values of average annual energy are estimated from the expected capacity factor for the given Generation Type within a given provincial electricity system. |  |  |  |
|  | Calculations | Average Annual Energy \= Capacity Factor \* Installed Capacity\*365\*24\*1000 |  |  |  |
|  | Limitations | Average annual energy can and does vary substantially from year to year due to environmental, system operating and demand factors. |  |  |  |
| Sources |  | AB | 1, 4, 12, 28, 42, 242, 244 | NS | 176, 189, 205 |
|  |  | BC | 28, 29, 243, 244 | ON | 101 |
|  |  | MB | 28, 109, 113, 117, 120 | PE | 53, 128, 130 |
|  |  | NB | 44, 53, 105, 150, 152 | QC | 79, 158, 192, 214 |
|  |  | NL | 159, 161, 165, 168, 212 | SK | 220, 226, 227 |

| Facility Effective Capacity (MW) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The capacity of a Generation Facility available at peak demand after derating based on the Capacity Adjustment. |  |  |  |
| Clarifications | None |  |  |  |
| Calculations | Unit Effective Capacity \= Unit Installed Capacity \* Capacity Adjustment |  |  |  |
| Limitations | The Facility Effective Capacity depends upon the Capacity Adjustment, which changes with the addition (removal) of generation assets. |  |  |  |
| Sources | AB | 1, 28, 42, 192, 242, 244 | NS | 28, 187, 192, 195, 205 |
|  | BC | 23, 24, 28, 29, 98, 192, 243, 244 | ON | 28, 98, 101, 102, 103, 244 |
|  | MB | 28, 113, 120, 140, 146, 192 | PE | 28, 98, 130, 192, 196 |
|  | NB | 28, 53, 150, 152, 156, 192 | QC | 28, 84, 87, 90, 98, 192, 214, 215, 244 |
|  | NL | 165, 172, 212 | SK | 28, 87, 140, 192, 220, 226, 227, 230 |

| Facility Installed Capacity (MW) |  |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
| Definition | Installed generation capacity of a Generation Facility. |  |  |  |  |
| Clarifications | None.  |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |  |
| Limitations | None. |  |  |  |  |
| Sources |  | AB | 1, 42, 242, 244 | NS | 187, 195 |
|  |  | BC | 24, 28, 29, 243, 244 | ON | 101, 103, 244 |
|  |  | MB | 113, 120, 146 | PE | 130, 196 |
|  |  | NB | 150, 152, 156, 198 | QC | 84, 90, 214, 215, 244 |
|  |  | NL | 165, 172, 212 | SK | 220, 226, 227, 230 |

| Fixed O&M Costs ($/MW-year) |  |
| :---- | :---- |
| Definition | Operations and maintenance costs of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility that do not vary with changes in energy generation. |
| Clarifications | Fixed operations and maintenance costs typically include labour costs associated with baseline staff, administration, dam maintenance, equipment and parts. They exclude costs that effect a substantial betterment of the generation facility. |
| Calculations | Where known, Fixed O&M Costs are directly obtained from sources. Where unknown, costs are estimated based on the known Fixed O&M Costs of other similar facilities. The current formula used in CODERS is as follows: Fixed O&M Costs \= 139832\*(Installed Capacity in MW)^-0.246 (hydro\_run) Fixed O&M Costs \= 573555\*(Installed Capacity in MW)^-0.4 (hydro\_daily) Fixed O&M Costs \= 2\*10^8\*(Installed Capacity in MW^1.372 (hydro\_monthly) Fixed O&M Costs \= \-13516ln(Installed Capacity in MW)+113199 (storage\_PH) Where known, Fixed O&M Costs for renewed hydroelectric projects vary substantially showing no pattern. Only the relevant costs (estimated to be 40% of all Fixed O&M costs based on Muskrat Falls O&M costs) associated with the renewal are considered. The current approach to hydroelectric renewal Fixed O&M Costs in CODERS is as follows: Fixed O&M Costs \= (Fixed Costs for the renewed project \- Fixed Costs for the existing project)/(Installed Capacity of the renewed project), where (using a hydro\_daily example): Fixed Costs for the existing project \= 573555\*(Existing Installed Capacity in MW)^-0.4\*(Existing Installed Capacity in MW) Fixed Costs for the renewed project \= (Fixed Costs for the existing project)\*(Existing Installed Capacity+(0.4)\*(Additional Installed Capacity))/(Existing Installed Capacity)  |
| Limitations | Estimates of Fixed O&M Costs may differ materially from actual Fixed O&M Costs. |
| Sources | 19, 28, 113, 142, 148, 245 |

| Forced Outage Rate (%) |  |
| :---- | :---- |
| Definition | The hours a Generation Facility, Storage Facility, Transmission Line or other facility is removed from service due to an unplanned condition divided by the total number of hours that the facility was removed from service plus the hours the facility was connected to the electricity system expressed as a percent.  |
| Clarifications | The forced outage rate excludes consideration of the hours of planned outage.  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 41, 203 |

| Generation Duration at Peak Output (h) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The total time that a pumped hydroelectric Storage Facility can continuously generate at its maximum generation capacity. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Generation Efficiency (%) |  |
| :---- | :---- |
| Definition | The ratio of total electrical energy produced by a thermal Generation Facility to the total fuel energy available. |
| Clarifications | The Generation Efficiency is an alternative means of expressing the heat rate of thermal Generation Facility. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 9, 51, 203 |

| Generation Facility Code |  |
| :---- | :---- |
| Definition | The unique CODERS asset identification code defining each generation facility and consisting of the following separated by an underscore: A two-letter provincial code A three-letter to five-letter generation facility code A three digit “GEN” defining this as a Generation Facility |
| Clarifications | Generation Facilities that consist of phases are presumed to be distinct Generation Facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility.  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | Efforts are made to utilize the generation asset identification codes used by the utilities and system operators within the Gen Unit Code. |
| Sources | N/A |

| Generation Facility Name |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The given name of a generation facility. |  |  |  |
| Clarifications | Generation facilities that consist of development phases are presumed to be distinct generation facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | A commonly used Generation Facility Name may differ from the legal name. Generation Unit Name may differ between sources. Alternative names are provided in the notes to each generation unit. \[CHECK that this is being done\] Generation facilities less \< 1 MW installed capacity are not included in CODERS. |  |  |  |
| Sources | AB | 42 | NS | 137, 187, 195 |
|  | BC | 23, 29, 57, 244 | ON | 101, 103, 244 |
|  | MB | 113, 120 | PE | 69, 130, 196 |
|  | NB | 44, 150, 152, 156, 244 | QC | 33, 84, 90, 214, 215, 244 |
|  | NL | 165, 172, 212 | SK | 220, 226, 227 |

| Generation Type |  |  |  |  |  |
| :---- | ----- | :---- | ----- | :---- | :---- |
| Definition | The type of electricity generation of a given generation unit, generation facility or generation contracted for import/export. |  |  |  |  |
| Clarifications | Generation types defined in CODERS include: |  |  |  |  |
|  | **Short Name** |  | **Description** |  |  |
|  | biogas |  | biogas |  |  |
|  | biomass |  | biomass |  |  |
|  | biomass\_CG |  | biomass cogeneration |  |  |
|  | coal |  | coal combined cycle |  |  |
|  | coal\_CCS |  | coal carbon capture and storage |  |  |
|  | diesel\_CT |  | diesel combustion turbine |  |  |
|  | diesel\_ST |  | diesel steam turbine |  |  |
|  | gasoline\_CT |  | gasoline combustion turbine |  |  |
|  | geothermal |  | geothermal binary cycle |  |  |
|  | hydro\_daily |  | hydroelectric daily storage |  |  |
|  | hydro\_storage |  | hydroelectric monthly storage |  |  |
|  | hydro\_run |  | hydroelectric non-storage |  |  |
|  | h2blue\_CT |  | hydrogen combustion turbine |  |  |
|  | h2green\_CT |  | hydrogen combustion turbine |  |  |
|  | MSW |  | municipal solid waste |  |  |
|  | NG\_CC |  | natural gas combined cycle |  |  |
|  | NG\_CCS |  | natural gas carbon capture and storage |  |  |
|  | NG\_CG |  | natural gas cogeneration |  |  |
|  | NG\_CT |  | natural gas combustion turbine |  |  |
|  | nuclear |  | nuclear light water reactor |  |  |
|  | nuclear\_SMR |  | nuclear small modular reactor |  |  |
|  | oil\_CT |  | oil combustion turbine |  |  |
|  | oil\_ST |  | oil steam turbine |  |  |
|  | solar\_PV |  | solar photovoltaic |  |  |
|  | solar\_recon |  | recontracted solar photovoltaic |  |  |
|  | storage\_lithium |  | lithium ion battery storage |  |  |
|  | storage\_pump |  | hydroelectric pumped storage |  |  |
|  | wind\_ons |  | onshore wind |  |  |
|  | wind\_recon |  | recontracted onshore wind |  |  |
|  | wind\_ofs |  | offshore wind |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |  |
| Limitations | None. |  |  |  |  |
| Sources | AB | 1, 238, 243, 244 |  | NS | 72, 184, 195 |
|  | BC | 23, 29, 57, 244 |  | ON | 101, 103, 244 |
|  | MB | 113, 120 |  | PE | 130, 196 |
|  | NB | 148, 150, 152 |  | QC | 33, 84, 90, 214, 215, 244 |
|  | NL | 165, 172, 212 |  | SK | 220, 226, 227 |

| Generation Type – COPPER |  |  |
| :---- | ----- | ----- |
| Definition | The type of electricity generation of a given generation unit, generation facility or generation contracted for import/export as used in the COPPER model. |  |
| Clarifications | Generation types defined in COPPER include: |  |
|  | **Type** | **Generation Types Included** |
|  | biomass | biogas, biomass, biomass co-generation, municipal solid waste |
|  | coal | coal |
|  | coal\_CCS | coal with carbon capture |
|  | diesel | diesel combustion turbine, diesel steam turbine, gasoline combustion turbine, oil combustion turbine, steam combustion turbine |
|  | geothermal | geothermal |
|  | hydro\_daily | hydro\_daily |
|  | hydro\_storage | hydro\_storage |
|  | hydro\_run | hydro\_run |
|  | h2blue\_CT | blue hydrogen combustion turbine |
|  | h2green\_CT | green hydrogen combustion turbine |
|  | NG\_CC | natural gas combined cycle |
|  | NG\_CCS | natural gas with carbon capture |
|  | NG\_CG | natural gas co-generation |
|  | NG\_CT | natural gas combustion turbine |
|  | nuclear | nuclear light water reactor |
|  | nuclear\_SMR | nuclear small modular reactor |
|  | solar\_PV | solar photovoltaic |
|  | solar\_recon | recontracted solar photovoltaic |
|  | wind\_ons | onshore wind |
|  | wind\_recon | recontracted onshore wind |
|  | wind\_ofs | offshore wind |
| Calculations | This is data directly obtained from sources. No calculations. |  |
| Limitations | None. |  |
| Sources | See Generation Type |  |

| Generation Unit Code |  |
| :---- | :---- |
| Definition | The unique CODERS asset identification code defining each generation unit and consisting of the following separated by an underscore: A two-letter provincial code A three-letter to five-letter generation facility code appended to a two-digit unit number A three digit “GEN” defining this as a Generation Unit |
| Clarifications | Generation Facilities that consist of phases are presumed to be distinct Generation Facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility.  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | Efforts are made to utilize the generation asset identification codes used by the utilities and system operators within the Gen Unit Code. |
| Sources | N/A |

| Generation Unit Name |  |
| :---- | :---- |
| Definition | Given name of a generation unit, consisting of the Generation Facility Name and the unit number. |
| Clarifications | Generation facilities that consist of development phases are presumed to be distinct generation facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility. A generation unit may represent a single physical generation unit where data is available to characterize that specific generation unit or may consist of all the generation units within a generation facility where unit-specific information is unavailable.  Onshore wind, offshore wind and solar facilities are always characterized as single generation units.  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | A commonly used Generation Facility Name may differ from the legal name. Generation Unit Name may differ between sources. Alternative names are provided in the notes to each generation unit. Generation facilities \< 1 MW installed capacity are not included in CODERS. |
| Sources | See Generation Facility Name |

| Greenfield Code |  |
| :---- | :---- |
| Definition | The unique CODERS asset identification code defining each Greenfield Project and consisting of the following separated by an underscore: A two-letter provincial code A three-letter to five-letter Generation Facility code A three digit “GRF” defining this as a Greenfield Project |
| Clarifications | Renewal Projects that consist of more than one capacity expansion are presumed to be distinct Generation Units in CODERS.  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | Efforts are made to utilize the generation asset identification codes used by the utilities and system operators within the Renewal Code. |
| Sources | N/A |

| Greenfield Project Name |  |
| :---- | :---- |
| Definition | Given name of a future potential hydroelectric development project that entails development of a new Generation Facility. |
| Clarifications | Generation facilities that consist of development phases are presumed to be distinct generation facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility. A generation unit may represent a single physical Generation Unit where data is available to characterize that specific generation unit or may consist of all the generation units within a generation facility where unit-specific information is unavailable. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | A commonly used Generation Facility Name may differ from the legal name. Generation Unit Name may differ between sources. Alternative names are provided in the notes to each generation unit. Generation Facilities \< 1 MW installed capacity are not included in CODERS. |
| Sources | See Capital Costs |

| Gross Head (m) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The total available head in a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications | Gross Head is the elevation difference between the surface of the upper reservoir and lower reservoir before consideration of losses. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Heat Rate (MMBtu/MWh) |  |
| :---- | :---- |
| Definition | The ratio of the total fuel energy available to the total electrical energy produced by a thermal Generation Facility. |
| Clarifications | The Generation Efficiency is an alternative means of expressing the heat rate of thermal Generation Facility. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 9, 51, 203 |

| Implementation Costs ($M) |  |
| :---- | :---- |
| Definition | The loaded capital costs of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility inclusive of Capital Overhead and Interest During Construction costs. |
| Clarifications | None. |
| Calculations | Implementation Costs \= Capital Costs \+ Capital Overhead \+ Interest During Construction |
| Limitations | See limitations related to Capital Costs, Capital Overhead and Interest During Construction. |
| Sources | See Capital Costs |

| Import Export Capacity (MW) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The firm capacity imported/exported between jurisdictions, or the amount (in MW) by which the total transfer capacity is reduced on the transmission lines between the jurisdictions that are party to the contract. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | Contracts for seasonal capacity exchanges are not included in CODERS. |  |  |  |
| Sources | AB | N/A | NS | 186 |
|  | BC | N/A | ON | 83 |
|  | MB | 125 | PE | 130 |
|  | NB | 198 | QC | 83, 88 |
|  | NL | 165, 212 | SK | 125 |

| Interest During Construction ($M) |  |
| :---- | :---- |
| Definition | Interest on debt accumulated during the construction period of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility and added to the cost of that facility. |
| Clarifications | None. |
| Calculations | Interest During Construction (IDC) \= PV\*(1+i)^n (Excel FV(rate,nper,pmt)), where Present value (PV) \= Capital Cost Interest rate (i) \= 5% Number of periods (n) \= Construction Time (years) |
| Limitations | Actual IDC rates may vary from 5% during the construction period. Capital costs are presumed to be expended equally throughout the construction period, which may overestimate or underestimate actual interest costs. |
| Sources | See Capital Costs |

| Latitude / Longitude (dd.dddd) |  |
| :---- | :---- |
| Definition | The latitude and longitude at which a generation, storage, transmission, or other facility is physical located. |
| Clarifications | None. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | The coordinate reference system differs between various sources and may result in slight differences in latitude and longitude between sources. |
| Sources | 62, 244 |

| Location |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The geographical location (town, city, river, etc.) in which a generation, storage, transmission, or other facility is physical located. |  |  |  |
| Clarifications | Some facilities are physically located in one province but interconnected to and operating within a neighbouring province. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 42 | NS | 137, 184, 185, 188 |
|  | BC | 23, 29, 57 | ON | 103, 206 |
|  | MB | 120 | PE | 69, 196 |
|  | NB | 44, 150, 152, 156, 244 | QC | 33, 84, 90, 214, 215, 244 |
|  | NL | 165, 172, 212 | SK | 220, 226, 227 |

| Maximum Capacity Factor (%) |  |
| :---- | :---- |
| Definition | The maximum annual energy production of a Generation Facility as a proportion of the total potential annual energy production. |
| Clarifications | The total potential annual energy production reflects the installed capacity multiplied by the annual hours the facility is available for generation. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 47, 48, 205 |

| Minimum Capacity Factor (%) |  |
| :---- | :---- |
| Definition | The minimum annual energy production of a Generation Facility as a proportion of the total potential annual energy production. |
| Clarifications | The total potential annual energy production reflects the installed capacity multiplied by the annual hours the facility is available for generation. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 47, 48, 205 |

| Minimum Down Time (hours) |  |
| :---- | :---- |
| Definition | The minimum time following ramping to zero generation capacity that a Generation Facility must remain before restarting. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 61, 104 |

| Minimum Generation (%) |  |
| :---- | :---- |
| Definition | The minimum generation of existing hydroelectric, hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facilities designed to account for requirements for minimum daily and seasonal downstream environmental flow requirements present at most hydroelectric facilities. |
| Clarifications | The Minimum Generation of existing hydroelectric, hydroelectric renewal and greenfield hydroelectric is presumed to be 10% of installed capacity, while the Minimum Generation of pumped storage hydroelectric is presumed to be 0% of installed capacity. |
| Calculations | This data is estimated. No calculations. |
| Limitations | The use of a generic 10% minimum generation for hydroelectric facilities may vary annually, seasonally, or daily compared to actual conditions. |
| Sources | N/A |

| Minimum Plant Load (%) |  |
| :---- | :---- |
| Definition | The minimum instantaneous operating capacity of a Generation Facility expressed as a proportion of the total installed capacity. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 201 |

| Minimum Up Time (hours) |  |
| :---- | :---- |
| Definition | The minimum time following ramping to desired generation capacity that a Generation Facility must operate. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 61, 104 |

| Number of Circuits |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The number of transmission circuits on a given transmission line or line segment. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 3 | NS | 180, 184 |
|  | BC | 30, 55 | ON | 78 |
|  | MB | 109, 111, 112, 114, 115, 116, 118, 121, 122, 138 | PE | 130 |
|  | NB | 150, 151, 231 | QC | 88 |
|  | NL | 144, 166, 173, 212 | SK | 216 |

| Number of Generation Units Represented  |  |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
| Definition | The physical number of generation units represented in the data respecting a generation unit or generation facility. |  |  |  |  |
| Clarifications | None. |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |  |
| Limitations | None. |  |  |  |  |
| Sources |  | AB | 42, 146 | NS | 146, 191 |
|  |  | BC | 28, 29, 146, 243, 244 | ON | 101, 146, 206 |
|  |  | MB | 120, 146 | PE | 130, 146 |
|  |  | NB | 44, 146, 152 | QC | 84, 146, 214, 215 |
|  |  | NL | 146, 172, 212 | SK | 146, 226 |

| Operating Region |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The Province served by the operation of a generation, storage, transmission, or other facility. |  |  |  |
| Clarifications | Some facilities are physically located in one province but interconnected to and operating within a neighbouring province. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 3 | NS | 137, 184, 185, 188 |
|  | BC | 23, 29, 57 | ON | 103, 206 |
|  | MB | 120 | PE | 69, 196 |
|  | NB | 150, 151, 231 | QC | 82, 90 |
|  | NL | 165, 172, 212 | SK | 220, 226, 227 |

| Owner |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The legal owner(s) of a generation, storage, transmission, or other facility. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 42 | NS | 137, 184, 187, 194 |
|  | BC | 23, 29, 57 | ON | 103, 244 |
|  | MB | 120, 146 | PE | 69, 196 |
|  | NB | 44, 150, 152, 156, 244 | QC | 84, 146, 214, 215, 236 |
|  | NL | 165, 172, 212 | SK |  |

| Penstock Length (m) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The total length of the water conduit used in a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Planned Outage Rate (%) |  |
| :---- | :---- |
| Definition | The ratio of annual unserved energy (MWh) due to planned work either on a Generation Facility, Storage Facility or transmission facility unit or component, to the installed plant capacity (MW) multiplied by 8760 hours. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 41, 203 |

| Possible Renewal Year |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The year a generation or storage facility is scheduled for major refurbishment or contract renewal, or a contract for import/export is scheduled for renewal. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 5, 10, 239, 243 | NS | 177, 186, 187, 191, 192 |
|  | BC | 23, 29, 40, 58, 242 | ON | 103, 206 |
|  | MB | 121, 123 | PE | 43, 69, 130, 211 |
|  | NB | 148, 149, 156 | QC | 33, 90, 236 |
|  | NL | 157, 165, 173, 212 | SK | 220, 226, 227, 230 |

| Previous Renewal Year |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The year when a generation or storage facility most recently underwent a major refurbishment or turbine-generator replacement, or a contract for import/export was last renewed.  |  |  |  |
| Clarifications | The Previous Renewal Year may be the same as the Start Year if the facility or contract has yet to be renewed. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 5, 10, 239, 243, 244 | NS | 137, 186, 187, 191, 192, 195 |
|  | BC | 29, 58 | ON | 103, 206 |
|  | MB | 113, 120, 121 | PE | 69, 130, 211 |
|  | NB | 44, 149, 150, 156 | QC | 33, 79, 84, 136, 214, 215, 236 |
|  | NL | 157, 165, 173, 212 | SK | 220, 226, 227, 228, 230 |

| Project Definition Costs ($M) |  |
| :---- | :---- |
| Definition | Costs associated with the planning, environmental assessment, and regulatory approval of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility |
| Clarifications | Project Definition Costs are typically deferred for inclusion in the rate base following commissioning of a Generation facility. |
| Calculations | Where known, Project Definition Costs are directly obtained from sources. Where unknown, costs are estimated based on the known Project Definition Costs of other similar facilities. The current formula used in CODERS is as follows: Project Definition Costs \= 0.9539\*(Implementation Costs)^0.6784 |
| Limitations | Estimates of Project Definition Costs may differ materially from actual Project Definition Costs. |
| Sources | See Capital Costs |

| Project Footprint (ha) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The total land area of the components of a pumped hydro Storage Facility |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 18, 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Province |  |
| :---- | :---- |
| Definition | The Province in which a generation, storage, transmission, or other facility is physical located. |
| Clarifications | Some facilities are physically located in one province but interconnected to and operating within a neighbouring province. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | 62, 244 |

| Pump Type |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The type of pump or pump-turbine (e.g., single-stage, multi-stage) used in a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | 18, 19, 240 |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Pumping Duration and Peak Output (h) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The total continuous time that a pumped hydroelectric Storage Facility can continuously pump at its maximum pump capacity. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Ramp Rate (%/minute) |  |
| :---- | :---- |
| Definition | The rate at which a Generation Facility can increase generation expressed as a proportion of the maximum increase in generating capacity per minute to the total installed generation capacity. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 61 |

| Reactance (ohm) |  |
| :---- | :---- |
| Definition | The opposition, essentially inertia, against the flow of alternating current by inductance (L) and capacitance (C). Along with resistance, it is one of the two elements of impedance.  |
| Clarifications | Impedance is a comprehensive expression of all forms of opposition to current flow, including both resistance and reactance. |
| Calculations | Surge impedance (Zc) \= (L/C)^0.5 Surge impedance loading (SIL) \= Voltage\*Voltage/Zc/10^6 |
| Limitations | None. |
| Sources | 14, 71, 209 |

| Renewal Project Name |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | Given name of a future potential hydroelectric development project that entails capacity expansion at an existing Generation Facility. |  |  |  |
| Clarifications | Generation facilities that consist of development phases are presumed to be distinct generation facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility. A generation unit may represent a single physical Generation Unit where data is available to characterize that specific generation unit or may consist of all the generation units within a generation facility where unit-specific information is unavailable. Renewal Projects are given a Generation Unit name consistent with the naming of existing units at the Generation Facility.  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | A commonly used Generation Facility Name may differ from the legal name. Generation Unit Name may differ between sources. Alternative names are provided in the notes to each generation unit. Generation facilities \< 1 MW installed capacity are not included in CODERS. |  |  |  |
| Sources | AB | 3 | NS | N/A |
|  | BC | 28 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | 80 |
|  | NL | 165 | SK | N/A |

| Renewal Code |  |
| :---- | :---- |
| Definition | The unique CODERS asset identification code defining each Renewal Project and consisting of the following separated by an underscore: A two-letter provincial code A three-letter to five-letter Generation Facility code A three digit “REN” defining this as a Renewal Project |
| Clarifications | Renewal Projects that consist of more than one capacity expansion are presumed to be distinct Generation Units in CODERS.  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | Efforts are made to utilize the generation asset identification codes used by the utilities and system operators within the Renewal Code. |
| Sources | N/A |

| Reservoir Storage (m3) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The storage volume of the limiting (usually upper) reservoir in a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 18, 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Reservoir Type |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The type of reservoir arrangement (lake to lake, lake to reservoir, reservoir to lake, reservoir to reservoir) used in a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 18, 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | 148 | QC | N/A |
|  | NL | N/A | SK | N/A |

| Round Trip Efficiency (%) |  |
| :---- | :---- |
| Definition | The ratio of energy (in MWh) obtained from storage to the energy (in MWh) placed into storage.  |
| Clarifications | The round-trip efficiency of pumped hydroelectric storage is assumed to be 80% unless it is otherwise known. |
| Calculations | No calculations. |
| Limitations | Estimates of Round Trip Efficiency may differ materially from actual Round Trip Efficiency. |
| Sources | N/A |

| Service Life (years) |  |
| :---- | :---- |
| Definition | The operational life of a Generation Facility, Storage Facility or other asset from the date of commissioning until replacement or retirement. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 17, 203, 205, 237 |

| Shut Down Costs ($/MW Cap) |  |
| :---- | :---- |
| Definition | The cost for a Generation Facility to ramp from operating conditions to shutdown (cold) conditions, expressed as a ratio of total costs divided by installed capacity. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 202, 243 |

| Spinning Reserve Capability (%) |  |
| :---- | :---- |
| Definition | The proportion of a Generation Facility installed capacity available for use as spinning reserve. |
| Clarifications | Spinning Reserve capacity is not available for energy production other than when it is called on to meet reserve requirements. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 41, 201 |

| Start Year |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The year when a generation or storage facility was commissioned or a contract for import/export was initiated. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 5, 42, 242, 243 | NS | 137, 186, 187, 191, 192, 195 |
|  | BC | 23, 29, 40, 57, 242 | ON | 103, 206 |
|  | MB | 113, 120 | PE | 69, 130, 211 |
|  | NB | 44, 149, 150, 156 | QC | 33, 79, 84, 136, 214, 215, 236 |
|  | NL | 165, 212 | SK | 220, 226, 227, 230 |

| Storage Capacity (MW) |  |
| :---- | :---- |
| Definition | The total possible instantaneous discharge capability of the energy storage facility. |
| Clarifications | None. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | See Storage Facility Name |

| Storage Code |  |
| :---- | :---- |
| Definition | The unique CODERS asset identification code defining each Storage Facility and consisting of the following separated by an underscore: A two-letter provincial code A three-letter to five-letter storage facility code A three digit “STO” defining this as a Storage Facility |
| Clarifications | Generation facilities that consist of phases are presumed to be distinct generation facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility.  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | Efforts are made to utilize the generation asset identification codes used by the utilities and system operators within the Gen Unit Code. |
| Sources | N/A |

| Storage Duration (hours) |  |
| :---- | :---- |
| Definition | The amount of time a storage facility can discharge at its storage capacity before depleting its storage energy. |
| Clarifications | None. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | See Storage Facility Name |

| Storage Energy (MWh) |  |
| :---- | :---- |
| Definition | The maximum amount of storage energy in a storage facility. |
| Clarifications | None. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | See Storage Facility Name |

| Storage Facility Name |  |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
| Definition | Given name of a Storage Facility. |  |  |  |  |
| Clarifications | Generation facilities that consist of development phases are presumed to be distinct generation facilities in CODERS unless they are interconnected to the same node and known to operate as a single facility. A generation unit may represent a single physical generation unit where data is available to characterize that specific generation unit or may consist of all the generation units within a generation facility where unit-specific information is unavailable.  Onshore wind, offshore wind and solar facilities are always characterized as single generation units.  |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |  |
| Limitations | A commonly used Generation Facility Name may differ from the legal name. Generation Unit Name may differ between sources. Alternative names are provided in the notes to each generation unit.  Storage facilities \< 1 MW installed capacity are not included in CODERS. |  |  |  |  |
| Sources |  | AB | 246 | NS | N/A |
|  |  | BC | N/A | ON | 94, 244 |
|  |  | MB | N/A | PE | 130 |
|  |  | NB | N/A | QC | N/A |
|  |  | NL | N/A | SK | 227 |

| Storage Type |  |  |
| :---- | ----- | ----- |
| Definition | The type of electricity generation of a given generation unit, generation facility or generation contracted for import/export. |  |
| Clarifications | Storage types defined in CODERS include: |  |
|  | **Short Name** | **Description** |
|  | storage\_air | compressed air energy storage |
|  | storage\_lithium | lithium-ion battery storage |
|  | storage\_pump | hydroelectric pumped storage |
|  | storage\_solid | solid-state energy storage |
| Calculations | This is data directly obtained from sources. No calculations. |  |
| Limitations | Detailed technical information is not currently developed in CODERS for compressed air and solid-state energy storage. |  |
| Sources | See Storage Facility Name |  |

| Storage Type – COPPER |  |  |
| :---- | ----- | ----- |
| Definition | The type of Storage Facility as used in the COPPER model. |  |
| Clarifications | Storage types defined in COPPER include: |  |
|  | **Type** | **Storage Types Included** |
|  | storage\_lithium | lithium-ion battery storage, solid-state energy storage, compressed air energy storage |
|  | storage\_pump | hydroelectric pumped storage |
| Calculations | This is data directly obtained from sources. No calculations. |  |
| Limitations | Detailed technical information is not currently developed in CODERS for compressed air and solid-state energy storage. |  |
| Sources | See Storage Facility Name |  |

| Time to Full Capacity (minutes) |  |
| :---- | :---- |
| Definition | The time required for a Generation Facility to ramp from idle (cold) conditions to full installed generation capacity. |
| Clarifications |  |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | These are generic values and may differ considerably from actual values. |
| Sources | 46 |

| Total Number of Facility Generation Units |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The aggregate physical number of generation units in a Generation Facility. |  |  |  |
| Clarifications | Not all generation units may be currently operational but are still included in the dataset. Decommissioned units are identified by their Closure Year. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 42, 146 | NS | 146, 191 |
|  | BC | 28, 29, 146, 243, 244 | ON | 101, 146, 206 |
|  | MB | 120, 146 | PE | 130, 146 |
|  | NB | 44, 146, 152 | QC | 84, 146, 214, 215 |
|  | NL | 146, 172, 212 | SK | 146 |

| Total Project Costs ($M) |  |
| :---- | :---- |
| Definition | The sum of Capital Costs, Capital Overhead, Interest During Construction and Project Definition Costs of a hydroelectric renewal, greenfield hydroelectric or pumped storage hydroelectric generation facility. |
| Clarifications | None. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | See limitations related to Capital Costs, Capital Overhead, Interest During Construction and Project Definition Costs. |
| Sources | See Capital Costs |

| Total Transfer Capacity (MW) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The thermal transfer capacity of a transmission line. |  |  |  |
| Clarifications | The Total Transfer Capacity of a transmission line varies with ambient temperature and is higher in winter compared to summer. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 8 | NS | 199 |
|  | BC | 27 | ON | 93 |
|  | MB | 126 | PE | 199 |
|  | NB | 199 | QC | 197 |
|  | NL | 199 | SK | 219 |
|  | CA | 37 |  |  |

| Transmission Circuit ID |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The unique utility issued identification associated with a transmission line. |  |  |  |
| Clarifications | None. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 3 | NS | 180, 184 |
|  | BC | 30, 55, 60 | ON | 78 |
|  | MB | 109, 111, 112, 114, 115, 116, 118, 121, 122, 138 | PE | 130 |
|  | NB | 106, 150 | QC | 85 |
|  | NL | 144, 166, 173, 212 | SK | 216 |

| Transmission Line Segment Length (km) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The physical length of a portion of a transmission line between to nodes |  |  |  |
| Clarifications | Transmission Line Segment Length may be sourced, measured using available maps where the routing is known or estimated based on the distance between nodes. |  |  |  |
| Calculations | This is data directly obtained from sources and maps. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 3, 62 | NS | 179, 194 |
|  | BC | 30, 55, 65 | ON | 62 |
|  | MB | 62, 110, 111, 112, 121, 122, 138 | PE | 130 |
|  | NB | 62, 231 | QC | 88 |
|  | NL | 157, 160, 173 | SK | 62 |

| Transmission Line Length (km) |  |
| :---- | :---- |
| Definition | The physical length of a transmission line, including all line segments between two nodes demarcating the end points of the line. |
| Clarifications | Transmission Line Length may be sourced, measured using available maps where the routing is known or estimated based on the distance between nodes. |
| Calculations | This is data directly obtained from sources and maps. No calculations. |
| Limitations | None. |
| Sources | See Transmission Line Segment Length |

| Turbine Type |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The type of turbine (e.g., Francis, Kaplan, Pelton) used in a hydroelectric facility or pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 240 | NS | N/A |
|  | BC | 18, 19 | ON | N/A |
|  | MB | N/A | PE | N/A |
|  | NB | N/A | QC | N/A |
|  | NL | N/A | SK | N/A |

| Unit Average Annual Energy (GWh/year) |  |
| :---- | :---- |
| Definition | The actual annual electrical energy output of a generation unit averaged over more than one year depending on data availability. |
| Clarifications | Some values of average annual energy are estimated from the expected capacity factor for the given Generation Type within a given provincial electricity system. |
| Calculations | Average Annual Energy \= Capacity Factor \* Installed Capacity\*365\*24\*1000 |
| Limitations | Average annual energy can and does vary substantially from year to year due to environmental, system operating and demand factors. |
| Sources | See Facility Annual Average Energy |

| Unit Effective Capacity (MW) |  |
| :---- | :---- |
| Definition | The capacity of a generation unit available at peak demand after derating based on the Capacity Adjustment. |
| Clarifications | None |
| Calculations | Unit Effective Capacity \= Unit Installed Capacity \* Capacity Adjustment |
| Limitations | The Unit Effective Capacity depends upon the Capacity Adjustment, which changes with the addition (removal) of generation assets. |
| Sources | See Facility Effective Capacity |

| Unit Installed Capacity (MW) |  |
| :---- | :---- |
| Definition | Installed generation capacity of a generation unit. |
| Clarifications | A “unit” reflects the number of generation units represented by the data, which may mean a single unit where unit-level data is available or all units where unit-level data is unavailable. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | See Facility Installed Capacity |

| Variable O&M Costs ($/MWh) |  |
| :---- | :---- |
| Definition | Operations and maintenance costs, excluding water rentals, of a Generation Facility that vary with changes in energy generation. |
| Clarifications | Variable operations and maintenance costs typically include labour costs associated with operations, consumable materials, production costs, and costs associated with cooling the facility. They exclude costs that affect a substantial betterment of the generation facility. |
| Calculations |  |
| Limitations | Estimates of Variable Costs may differ materially from actual Variable Costs. |
| Sources | 19, 28, 245 |

| Voltage (kV) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The electromotive force, also known as the electric pressure, or electric potential difference between two points. |  |  |  |
| Clarifications | Transmission line Voltages vary from 63 kV to 765 kV in CODERS. |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 3 | NS | 180, 184 |
|  | BC | 30, 55 | ON | 247 |
|  | MB | 109, 111, 112, 114, 115, 116, 118, 121, 122, 138 | PE | 130 |
|  | NB | 106 | QC | 85 |
|  | NL | 144, 166, 173, 212 | SK | 216 |

|  | Water Rentals ($/MWh) |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- | :---- |
|  | Definition | A form of variable operations and maintenance costs potentially levied as a tax on hydroelectric renewals, greenfield hydroelectric or pumped storage hydroelectric generation facilities based on energy production. |  |  |  |
|  | Clarifications | Only variable water rentals are included in CODERS, as fixed charges (e.g., land taxes) are not included separately for other types of generation. Not all provinces charge water rentals. |  |  |  |
|  | Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
|  | Limitations | None. |  |  |  |
| Sources |  | AB | N/A | NS | N/A |
|  |  | BC | 241 | ON | 67 |
|  |  | MB | 234 | PE | N/A |
|  |  | NB | N/A | QC | 70 |
|  |  | NL | 66 | SK | 235 |

| Water Type |  |  |  |  |
| :---- | :---- | :---- | :---- | :---- |
| Definition | The type of water (fresh or salt) used in a pumped hydroelectric Storage Facility. |  |  |  |
| Clarifications |  |  |  |  |
| Calculations | This is data directly obtained from sources. No calculations. |  |  |  |
| Limitations | None. |  |  |  |
| Sources | AB | 238, 240 | NS | N/A |
|  | BC | 18, 19 | ON | 174, 233 |
|  | MB | N/A | PE | N/A |
|  | NB | 148 | QC | N/A |
|  | NL | N/A | SK | N/A |

| Wind Project Area (km2) |  |
| :---- | :---- |
| Definition | The permitted land use area of a wind project. |
| Clarifications | Where source data is not available, the Wind Project Area is visually estimated using Google Maps. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | 62 |

| Wind Project Density (MW/km2) |  |
| :---- | :---- |
| Definition | The spatial density of the installed capacity of a wind Generation Facility. |
| Clarifications | None. |
| Calculations | Wind Project Density \= Facility Installed Capacity / Wind Project Area |
| Limitations | None. |
| Sources | N/A |

| Wind Turbine Capacity (MW) |  |
| :---- | :---- |
| Definition | The installed capacity of individual wind turbines within a wind Generation Facility. |
| Clarifications | None. |
| Calculations | This is data directly obtained from sources. No calculations. |
| Limitations | None. |
| Sources | 146 |


[image1]: img/image111.png
