Canadian Open-source Database for Energy Research and Systems-Modelling (CODERS) is an open-source database developed by the University of Victoria (Sustainable Energy Systems Integration and Transition Group, SESIT) and the University of Toronto (Dept. of Civil and Mineral Engineering). 

The recommended reference for CODERS is the following:
Hendriks, R.M., Monroe, J., Cusi, T., Aldana, D., Griffiths, K., Dorman, T., Chhina, A., McPherson, M. 2023.  Canadian Open-source Database for Energy Research and Systems-Modelling (CODERS). DOI/URL: TBD.