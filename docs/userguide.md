# **CODERS USER GUIDE**

- Version 2.0

- October 2023

-  Hendriks, R., Aldana, D., Cusi, T., Archibald, N.


## 1. **Introduction** {#introduction}

Canadian Open-source Database for Energy Research and Systems-Modelling (CODERS) is an open-source database developed by the University of Victoria (Sustainable Energy Systems Integration and Transition Group, SESIT) and the University of Toronto (Dept. of Civil and Mineral Engineering). The recommended reference for CODERS is the following:

Hendriks, R.M., Monroe, J., Cusi, T., Aldana, D., Griffiths, K., Dorman, T., Chhina, A., McPherson, M. 2023\.  Canadian Open-source Database for Energy Research and Systems-Modelling (CODERS). DOI/URL: TBD.

## 1.1 **Purpose** {#purpose}

The User Guide demonstrates the current capabilities of CODERS and provides the procedure for pulling data from the database.

## 1.2 **User Definition** {#user-definition}

A CODERS User is defined as someone who can only send HTTP requests to the API. This role restricts access to the MySQL database, API code, and backend servers that host the API and database. 

## 2 **Database Overview** {#database-overview}

## 2.1 **Data in CODERS** {#data-in-coders}

CODERS includes electricity system data related to generation, storage, transmission, substations and system operations, hourly and annual provincial electricity demand data, interprovincial and international electricity transfers and transmission interties, and spatial data related to Canada’s electricity networks.

For a full list of data stored in the database, refer to the CODERS Data Guide.

## 2.2 **Database Access** {#database-access}

CODERS was built using MySQL, an open-source relational database management system that uses Structured Query Language (SQL). A CODERS User has access to the database via an Application Programming Interface (API) which is the communication layer between the User and the database. A User does not have direct access to the MySQL database. Users will send HTTP requests to the API, and the requested data will be returned in JavaScript Object Notation (JSON) format. Although the API can be accessed via any web browser, a user will not be able to extract and manipulate that data unless it is accessed with a high-level programming language such as Python.

## 3 **Using the API** {#using-the-api}

## 3.1 **Accessing** {#accessing}

https://api.sesit.ca](https://api.sesit.ca/) 

The URL above can be used to access the various end-points of the API.

<!-- ## 3.2 **Installing Python** {#installing-python}

 The steps for installing Python are set out below.


| Install python on your local computer, The installation location does not matter. -Navigate to the Microsoft App Store (apps.microsoft.com), search, download and install the latest version of Python  -OR navigate to the Python website[Welcome to Python.org](https://www.python.org/)) and download the latest version. | ![A screenshot of a computerDescription automatically generated] [image1] | ![A screenshot of a computerDescription automatically generated] [image2] |
| :---- | :---: | :---: |
| Open command prompt/terminal, then type “cmd” | ![A screenshot of a computer errorDescription automatically generated with medium confidence][image3] |  |
| Type into the command line the following: “where python” | ![A computer screen with white textDescription automatically generated with low confidence][image4] |  |
| Copy the file path that appears before the python.exe  | ![][image5] |  |
| In the windows search bar, search and open the following application:  “Edit the system environment variables” \*\*\*IMPORTANT NOTE\*\*\*  **Do not** click on “Edit environment variables for your account”. This will not let you edit the path. | ![A screenshot of a computerDescription automatically generated][image6] |  |
| Click on “Environment Variables” | ![A screenshot of a computer programDescription automatically generated with low confidence][image7] |  |
| You should see the system variables and be able to edit them.  | ![A screenshot of a computerDescription automatically generated][image8] |  |
| If you made the mistake of clicking directly on “Edit environment variables for your account” the edit options will be greyed out | ![A screenshot of a computerDescription automatically generated][image9] |  |
| Click on “path” in the “System Variables” and then hit “Edit”. Double click on the system path variable, then click New | ![A screenshot of a computer programDescription automatically generated with medium confidence][image10] |  |
| Paste in the path of the python.exe and remove the python.exe at the end of the file path. Click ok and close the edit system variables windows.  | ![A screenshot of a computer programDescription automatically generated][image11] |  |
| You know that python is setup properly when you can use python and pip commands in the terminal. Type “python” in the terminal and the python version should show.   | ![A black background with white textDescription automatically generated][image12] |  |
| Next, we will install the python requests plugin. The Python *requests* library is a plugin that allows users to send HTTP requests to a given web address. In other words, this plugin makes it very easy to interact with CODERS.  **Installation**: The plugin is installed via pip as with most Python plugin installations. After the Python path is setup properly, enter the following command in your command terminal: pip install requests **Resources:** For the full documentation of this library, you can visit [https://requests.readthedocs.io/en/master/](https://requests.readthedocs.io/en/master/) | ![A computer screen shot of a programDescription automatically generated][image13] |  |

## 3.3 **Setting up Visual Studio Code**

| To get started, please contact the CODERS team *(listed the contact here)*. Send your IP address. Be aware that IP Addresses change on a regular basis, if you haven’t used the API in some time check your IP address first.  | [https://whatismyipaddress.com/](https://whatismyipaddress.com/)  |
| :---- | :---- |
| To check if you have access, use an internet browser and navigate to: **https://api.sesit.ca/** If access is granted, you will see the welcome screen. If you do not have access, the page will try to load for a while and then throw an error. | ![A screenshot of a computer screenDescription automatically generated][image14] |
| One you have access, download Visual Studio Code (VS Code). This is a free Microsoft application available for MAC, PC, and Linux, that is extremely useful for programming, setting up virtual environments, and most importantly, accessing the API.  [https://code.visualstudio.com/Download](https://code.visualstudio.com/Download) This User Guide explains the Windows setup, but Mac, and Ubuntu will have similar install procedures. | ![A penguin with blue text boxesDescription automatically generated with medium confidence][image15] |
| Once the setup file is downloaded, run that executable. Follow through the installer, and VS Code will ask where on your computer you want to store the main VS code files. After the install wizard is  | ![A blue logo with black textDescription automatically generated][image16] | 
| VS code has many useful plugins and tools, and we would recommend exploring them using a beginners guide to using VS Code or follow the virtual environment tutorial at the end of this document. While these features are not strictly necessary to use the CODERS API, they will assist when running simulations and accessing the API.  |  |

## 3.3.1 **Python Visual Studio Code Example**

This example demonstrates how a user would send an HTTP GET request in Python.

| To open a new file, click in the top left corner on file, then click new file. This will bring you over to the search bar at the top and allow you to choose the file type. Choose Python File. | ![A screenshot of a computerDescription automatically generated][image17] ![A screenshot of a computerDescription automatically generated][image18] |
| :---- | :---- |
| An empty python file will open. Copy the following code into the file: import requests \#import libraryx \= requests.get('https://api.sesit.ca/tables](about:blank)') \#send request print(x.json()) \#print response |  ![A screenshot of a computerDescription automatically generated][image19] |
| Save the file under any name in any location on your machine. The file should now say .py next to its name at the top.   | ![A screen shot of a computerDescription automatically generated][image20] |
| Execute the python code, click the little arrow run button on the top right-hand side. | ![A screenshot of a computerDescription automatically generated][image21] |
| This query of the API was x = requests.get('https://api.sesit.ca/tables](about:blank))' The requests.get command queries the API, and whatever the API returns is stored in the variable x. In this case, the Program is simply querying the API at https://api.sesit.ca/ and then it is asking “What is the list of tables at that IP?”  | ![A screen shot of a computer codeDescription automatically generated][image22] | The print(x.json()) line prints whatever is stored in the variable x, which happens to be a list of all the data tables.| -->

## 3.2 **Endpoints** {#endpoints}

The url '[https://api.sesit.ca/tables](about:blank)' is an example of an endpoint, and this section describes how to reach deeper into the API with more specific queries and to  access different endpoints.

An endpoint is a single URL representing a specific API service, and each endpoint is the location from which the API can access the resources needed to carry out their function. Below are some examples.

**https://api.sesit.ca/**

*  This endpoint returns a welcome message and a list of the current functions of the API.

**https://api.sesit.ca/filters**

*  This endpoint returns a list of the available search filters for each table in the database.

**https://api.sesit.ca/api/docs**

*  This endpoint is meant for use in a web browser. Enter the URI into the search bar, and you will be brought to a documentation page for the CODERS API. The page provides a full list of tables and information on the schema. 

**https://api.sesit.ca/tables**

*  This endpoint returns the list of tables that the user is able to access. This table is important because it shows the exact table names that must be used in order to access the data from those tables. Any discrepancies in the table name will result in a failed request. 

**https://api.sesit.ca/\[table\]**

*  This endpoint returns the entirety of the rows within the specified table. Below is the list of accessible tables along with the corresponding API table name. For a description of each table and the attributes within each table, refer to the provided Data Guide.

*  Generators: generators

*  Substations: substations

*  Transmission Lines: transmission\_lines

*  Storage Batteries: storage\_batteries

*  Interties: interties

*  Junctions: junctions

*  International Transfers: international\_transfers

*  Interprovincial Transfers: interprovincial\_transfers

*  Provincial Demand: provincial\_demand

*  Interface Capacity: interface\_capacity

*  Distribution Transfers: distribution\_transfers

*  Contribution Transfers: contribution\_transfers

*  Transfer Capacity \- COPPER: transfer\_capacity\_copper

*  CA System Parameters: CA\_system\_parameters

*  Hydro Capacity Factor: hydro\_capacity\_factor

*  Annual Demand and Efficiencies: annual\_demand\_and\_efficiencies

*  Generation Cost Evolution: generation\_cost\_evolution

*  Generic Generation: generation\_generic

*  Surface Area per Grid Cell: grid\_cell\_surface\_area

*  Intertie Capacity: intertie\_capacity

*  Regional Electrified Demand: regional\_electrified\_demand

*  Regional Heat Demand: regional\_heat\_demand

*  Generic Transmission: transmission\_generic

*  References: references

**https://api.sesit.ca/\[table\]/attributes**

*  This endpoint returns the list of attributes of the specified table. Like the endpoint above, the API returns an error if the given table does not match any of the accessible tables.

**https://api.sesit.ca/\[table\]?province=\[province\_code\]**

*  This endpoint returns the rows within the specified table filtered by province. There are a select number of tables that can be categorized by a province attribute. They are listed below. If one were to provide an invalid table or invalid province code, the request would fail.

*  Generators: generators

*  Substations: substations

*  Transmission Lines: transmission\_lines

*  Storage Batteries: storage\_batteries

*  Interties: interties

*  Junctions: junctions

**https://api.sesit.ca/international\_transfers?year=\[year\]&province=\[province\]&us\_region=\[region\]**

Example: https://api.sesit.ca/international\_transfers?year=2018\&province=QC-Highgate\&us\_region=ISONE

*  This endpoint returns the rows within the International Transfers table filtered by the year, province, and US state/region. If an error code is returned, there exists no data on the transfers between the specified province and US state during the given year. Below is a list of the current international transfer data within the CODERS database.

*  AB → US-Montana: 2018, 2019, 2020, 2021

*  BC → US-Northwest: 2018, 2019, 2020, 2021

*  NB → ISONE-Maine: 2018, 2019, 2020, 2021

*  NB → NMISA-NorthernMaine: 2018, 2019, 2020, 2021

*  ON → MISO-Michigan: 2018, 2019, 2020, 2021

*  ON → NYISO-West: 2018, 2019, 2020, 2021

*  ON → MISO-Minnesota: 2018, 2019, 2020, 2021

*  QC-Highgate → ISONE: 2018, 2019, 2020, 2021

*  QC-HVDC → ISONE: 2018, 2019, 2020, 2021

*  QC → NYISO-HQ: 2018, 2019, 2020, 2021

*  SK → MISO-Dakota: 2018, 2019, 2020, 2021

**https://api.sesit.ca/interprovincial\_transfers?year=\[year\]&province1=\[province\_1\]&province2=\[province\_2\]**

Example: https://api.sesit.ca/interprovincial\_transfers&year=2019\&province1=BC\&province2=AB

*  This endpoint returns the rows within the Interprovincial Transfers table filtered by the year, and two specified provinces. If an error code is returned, there exists no data on the transfers between the specified provinces during the given year. Below is a list of the current interprovincial transfer data within the CODERS database.

*  AB → SK: 2018, 2019, 2020, 2021

*  BC → AB: 2018, 2019, 2020, 2021

*  NB → NS: 2018, 2019, 2020, 2021

*  NB → PE: 2018, 2019, 2020, 2021

*  NB → QC: 2018, 2019, 2020, 2021

*  ON → MB: 2018, 2019, 2020, 2021

*  ON → QC: 2018, 2019, 2020, 2021

*  SK → MB: 2018, 2019, 2020, 2021

**https://api.sesit.ca/provincial\_demand?year=\[year\]&province=\[province\]**

Example: https://api.sesit.ca/provincial\_demand?year=2019\&province=BC

*  This endpoint returns the rows within the Provincial Transfers table filtered by the year and province. If an error code is returned, there exists no data on the demand of the specified province during the given year. Below is a list of the current provincial demand data.

*  AB: 2018, 2019, 2020, 2021

*  BC: 2018, 2019, 2020, 2021

*  MB: 2018, 2019, 2020, 2021 

*  NB: 2018, 2019, 2020, 2021

*  NL: 2018, 2019, 2020, 2021

*  NS: 2018, 2019, 2020, 2021

*  ON: 2018, 2019, 2020, 2021

*  PE: 2018, 2019, 2020, 2021 

*  QC: 2018, 2019, 2020, 2021

*  SK: 2018, 2019, 2020, 2021

**https://api.sesit.ca/generators?province=\[province\]&type=\[generator\_type\]**

Example: https://api.sesit.ca/generators?provine=AB\&type=hydro\_daily

*  This endpoint returns the generators of a specified type within a specified province. If an error code is returned, there are no generators of the specified type in the province provided or an invalid generator type was given. Below is a list of valid generator types.

*  wind

*  gas

*  biomass

*  coal

*  solar

*  hydro\_run

*  hydro\_pump

*  hydro\_daily

*  hydro\_monthly

*  diesel

*  nuclear

For a description of each of these generation types, refer to the CODERS Data Guide.

**https://api.sesit.ca/annual\_demand\_and\_efficiencies?region=\[region\]**

Example: https://api.sesit.ca/annual\_demand\_and\_efficiencies?region=Metro\_Vancouver

*  This endpoint brings you to the annual\_demand\_and\_efficiencies table which can be broken down into available regions. If the data on that region doesn’t exist, then an error is returned. Below is the list of available regions.

*  MetrVancouver

**https://api.sesit.ca/regional\_heat\_demand?year=\[year\]&region=\[region\]**

*  This endpoint returns the heat demand gathered at each hour of the given year. If the data does not exist for the given year and region, an error is returned. Below is a list of available regions.

*  MetrVancouver: 2016

**https://api.sesit.ca/regional\_electrified \_demand?year=\[year\]&region=\[region\]**

*  This endpoint returns the electrified demand gathered at each hour of the given year. If the data does not exist for the given year and region, an error is returned. Below is a list of available regions.

*  MetrVancouver: 2016

## 3.3 **Swagger API Documentation** {#swagger-api-documentation}

The [https://coders.cme-emh.ca](https://coders.cme-emh.ca) URI brings you to the Swagger UI documentation for the CODERS API. It is strongly recommended that you use this webpage as a compliment to the User Guide and Data Guide as it allows for a more interactive experience for learning about the functions of CODERS.

<!-- ## 3.4 **Spine Toolbox Example** {#spine-toolbox-example}

![A screenshot of a computerDescription automatically generated][image23]

**Figure 1**

Figure 1 above shows a Spine Toolbox tool labeled “get\_substations\_NL 1”. This tool contains a Python script that sends a HTTP GET request to the CODERS API.

![A computer screen with text on itDescription automatically generated][image24]

**Figure 2**

Figure 2 above shows the script contained within the “get\_substations\_NL 1” tool. 

![][image25] 

**Figure 3**

Figure 3 above shows data for a single substation from the request. The request results are provided in a JSON file.

The process outlined through Figures 1-3 is a simple case in which Spine Toolbox can access the API to request a specified class of data. Figure 1 shows how that script is modelled in Spine Toolbox. Figure 2 showcases the script contained within the tool, and Figure 3 shows the data returned in JSON format. The domain URI is a stand-in and is not the actual domain of the API. The URI endpoint tables/substations/NL returns the substations in Newfoundland and Labrador. The HTTP request is sent using the Python **requests** library, and the API receives the request to process and query the database. Once that data is obtained, it can be output to other scripts for use in energy systems models. The JSON data can also be converted to other data formats like CSV.  -->

<!-- ## 3.4 **Manipulating the JSON Data** {#manipulating-the-json-data}

JSON format is very similar to that of a Python dictionary. If the request was successful, the data will be formatted as a list of dictionaries. Each dictionary will consist of key-value pairs with each key mapping to its associated value. Below is how one would obtain that data from the API.

![][image26] 


The code above will print out the entirety of the data on the substations in Alberta. Now if you want to narrow down the data even further or find a specific column from each row, you can utilize the functions of a dictionary and search for a specified key. 

![][image27] 

![][image28] 


In the above example, the column name is used to add each substation in the Labrador region of Newfoundland and Labrador to a list. Each column in a table can be treated as a key, and the value of that column can be accessed with the ![][image29]  statement. This allows you to further narrow down your search. -->

## 3.4 **Setting up a Virtual Environment**

Using a virtual environment is not necessary for accessing the CODERS database but is extremely useful when running multiple different simulations. For example, let’s say you want to run two COPPER simulations, each using a different set of python plugins. A requirements.txt file specifies what version of python and its plugins to use for each simulation run. It is much simpler to switch from one environment to another then it is to have to edit the plugin versions, and input files every time you switch from one simulation to the other. Another benefit of a virtual environment is that it is isolated, so you don’t have to worry about keeping the simulation files separate. 

<!-- ## 3.7.1 **Creating the Virtual Environment** {#creating-the-virtual-environment}

Virtual environments make it easy to keep simulations and plugins organized. Requirements files outline all the plugins for that environment run, thus reducing many complications that can arise from conflicting package versions. The following instructions will be followed using Visual Studio Code, but any IDE will work.

[https://realpython.com/python-virtual-environments-a-primer/](https://realpython.com/python-virtual-environments-a-primer/)

| Create a folder in the desired location on your computer where you want the virtual environment to live. | ![][image30] |
| :---- | :---- |
| Open the Command line/terminal and Navigate to that folder. | ![][image31] ![][image32]  |
| Run the following venv command to create a new virtual Python environment:           *python \-m venv your-venv-name-goes-here* |  ![][image33] |
| The above command will not return any output. You may have to wait a few seconds and try typing or hitting enter to get a new line.  To check if the virtual environment has been created properly, navigate to the folder location. The folder will have fresh python files.  Have a look through the folders to familiarize yourself. The Lib folder, for example, contains the default plugins and any new ones that we install.  | ![A white background with black textDescription automatically generated][image34]  |

## 3.7.2 **Activating the virtual environment** {#activating-the-virtual-environment}

| In the command line, navigate to the Script folder | ![][image35] |
| :---- | :---- |
| Run the file *activate.bat*  | ![][image36] |
| The command line will now show the virtual environment that is currently active to the left of your file path. | ![][image37]  |
| Let’s install the requests plugin:  pip install requests   | ![A screen shot of a computer programDescription automatically generated][image38] |
| Checking the Lib file in our file explorer, we can see that the requests plugin library is now part of the virtual environment. This plugin will be active for this environment only, once again highlighting the power of virtual environments | ![A screenshot of a computerDescription automatically generated][image39] |

## 3.7.3 **Deactivating or changing the virtual environment** {#deactivating-or-changing-the-virtual-environment}

| Repeat the steps for creating a new virtual environment. As you can see, I did not need to exit or deactivate my first virtual environment to make a second one.   | ![][image40] ![A blue and white lineDescription automatically generated][image41] |
| :---- | :---- |
| Navigate to the scripts folder of the new virtual environment.  | ![A black background with white textDescription automatically generated][image42]  |
| Be careful that you are in the correct file path. Run *activate.bat*  |  ![A black background with white textDescription automatically generated][image43] |
| Now you can see that you are in the second virtual environment you created. Any plugins you install here will stay separated from the original virtual environment. As you can see, the requests library is not in this second virtual environment yet.  | ![A screenshot of a computerDescription automatically generated][image44]  |
| To deactivate a virtual environment, you can either change to a new environment as shown above, or once again navigate to the scripts folder and run the *deactivate.bat* file. | ![A black background with white textDescription automatically generated][image45] | -->

<!-- ## 3.8 **Installing Cplex** 

Cplex is an efficient **linear solver,** and requires an academic email to access. Please see 

![A screenshot of a computer programDescription automatically generated][image46] -->

[image1]: img/image1.png
[image2]: img/image2.png
[image3]: img/image3.png
[image4]: img/image4.png
[image5]: img/image5.png
[image6]: img/image6.png
[image7]: img/image7.png
[image8]: img/image8.png
[image9]: img/image9.png
[image10]: img/image10.png
[image11]: img/image11.png
[image12]: img/image12.png
[image13]: img/image13.png
[image14]: img/image14.png
[image15]: img/image15.png
[image16]: img/image16.png
[image17]: img/image17.png
[image18]: img/image18.png
[image19]: img/image19.png
[image20]: img/image20.png
[image21]: img/image21.png
[image22]: img/image22.png
[image23]: img/image23.png
[image24]: img/image24.png
[image25]: img/image25.png
[image26]: img/image26.png
[image27]: img/image27.png
[image28]: img/image28.png
[image29]: img/image29.png
[image30]: img/image30.png
[image31]: img/image31.png
[image32]: img/image32.png
[image33]: img/image33.png
[image34]: img/image34.png
[image35]: img/image35.png
[image36]: img/image36.png
[image37]: img/image37.png
[image38]: img/image38.png
[image39]: img/image39.png
[image40]: img/image40.png
[image41]: img/image41.png
[image42]: img/image42.png
[image43]: img/image43.png
[image44]: img/image44.png
[image45]: img/image45.png
[image46]: img/image46.png
